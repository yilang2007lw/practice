#!/usr/bin/env python

from twisted.protocols.ftp import FTPClient, FTPFileListProtocol
from twisted.internet.protocol import Protocol, ClientCreator
from twisted.python import usage
from twisted.internet import reactor

import string
import sys

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

class BufferingProtocol(Protocol):
    def __init__(self):
        self.buffer = StringIO()

    def dataReceived(self, data):
        self.buffer.write(data)

def success(response):
    print "Success"
    if response is None:
        print None
    else:
        print string.join(respoinse, '\n')

def fail(error):
    print "Failed"
    print error

def showFiles(result, fileListProtocol):
    print "Processed file listing"
    for file in fileListProtocol.files:
        print "%s:%d bytes, %s" % (file['filename'], file['size'], file['date'])
    print "total:%d files" % (len(fileListProtocol.files))

def showBuffer(result, bufferProtocol):
    print "got data"
    print bufferProtocol.buffer.getvalue()

class Options(usage.Options):
    optParameters = [['host', 'h', 'localhost'],
                     ['port', 'p', 21],
                     ['username', 'u', 'anonymouns'],
                     ['password', None, 'twisted@'],
                     ['passive', None, 0],
                     ['debug', 'd', 1],
                    ]

def run():
    config = Options()
    config.parseOptions()
    config.opts['port'] = int(config.opts['port'])
    config.opts['passive'] = int(config.opts['passive'])
    config.opts['debug'] = int(config.opts['debug'])

    FTPClient.debug = config.opts['debug']
    creator = ClientCreator(reactor, FTPClient, config.opts['username'],
                config.opts['password'], passive = config.opts['passive'])

    creator.connectTCP(config.opts['host'], config.opts['port']).addCallback(connectionMade).addErrback(connectionFailed)
    reactor.run()

def connectionFailed(f):
    print "connection failed", f
    reactor.stop()

def connectionMade(ftpClient):
    ftpClient.pwd().addCallbacks(success, fail)

    fileList = FTPFileListProtocol()
    d = ftpClient.list('.', fileList)
    d.addCallbacks(showFiles, fail, callbackArgs = (fileList,))
    
    ftpclient.cdup().addCallbacks(success, fail)
        
    proto = BufferingProtocol()

    d = ftpClient.nlst('.', proto)
    d.addCallbacks(showBuffer, fail, callbackArgs=(proto,))
    d.addCallback(lambda result: reactor.stop())

if __name__ == "__main__":
    run()
