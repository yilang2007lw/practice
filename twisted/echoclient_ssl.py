#!/usr/bin/env python

from OpenSSL import SSL
import sys

from twisted.internet.protocol import ClientFactory
from twisted.protocols.basic import LineReceiver
from twisted.internet import ssl, reactor

class EchoClient(LineReceiver):
    end = "Bye-Bye"
    def connectionMade(self):
        self.sendLine("Hello, World");
        self.sendLine("What a fine day it is");
        self.sendLine(self.end)

    def connectionLost(self, reason):
        print "connection lost"

    def lineReceived(self, line):
        print "receive:", line
        if line == self.end:
            self.transport.loseConnection()

class EchoClientFactory(ClientFactory):
    protocol = EchoClient

    def clientConnectionFailed(self, connector, reason):
        print "connection failed:", reason.getErrorMessage()
        reason.stop()

    def clientConnectionLost(self, connector, reason):
        print "connection lost:", reason.getErrorMessage()
        reactor.stop()

def main():
    factory = EchoClientFactory()
    reactor.connectSSL('localhost', 8000, factory, ssl.ClientContextFactory())
    reactor.run()

if __name__ == "__main__":
    main()
