#include "rygel-renderer-gst.h"
#include "rygel-core.h"

int main(int argc, char *argv[])
{
    GstElement *playbin, *sink, *asink;
    RygelPlaybinRenderer *renderer;
    GError *error = NULL;
    GMainLoop *loop;

    gst_init (&argc, &argv);

    g_set_application_name ("Standalone-Renderer");

    renderer = rygel_playbin_renderer_new ("LibRygel renderer demo");
    playbin = rygel_playbin_renderer_get_playbin (renderer);
    sink = gst_element_factory_make ("cacasink", NULL);
    g_object_set (G_OBJECT (sink),
                  "dither", 53,
                  "anti-aliasing", TRUE,
                  NULL);

    asink = gst_element_factory_make ("pulsesink", NULL);

    g_object_set (G_OBJECT (playbin),
                  "video-sink", sink,
                  "audio-sink", asink,
                  NULL);

    if (argc >= 2) {
        rygel_media_device_add_interface (RYGEL_MEDIA_DEVICE (renderer), argv[1]);
    } else {
        rygel_media_device_add_interface (RYGEL_MEDIA_DEVICE (renderer), "eth0");
    }

    loop = g_main_loop_new (NULL, FALSE);
    g_main_loop_run (loop);

    return 0;
}

