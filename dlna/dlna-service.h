#ifndef DLNA_SERVICE_H
#define DLNA_SERVICE_H

#include <glib.h>
#include <libgupnp/gupnp.h>

void print_service_info (GUPnPServiceInfo *service_info);

void test_state_variable (GUPnPServiceInfo *service_info);

void test_content_directory (GUPnPServiceProxy *proxy);

#endif
