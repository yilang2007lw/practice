#include "dlna-cp.h"
#include "dlna-service.h"
#include <libsoup/soup.h>

void
print_device_info (GUPnPDeviceProxy *proxy)
{

    GUPnPDeviceInfo *info = GUPNP_DEVICE_INFO (proxy);
    g_assert (info != NULL);

    g_printf ("Here prints device info\n");

    g_printf ("device info location:%s\n", gupnp_device_info_get_location (info));
    g_printf ("device info udn:%s\n", gupnp_device_info_get_udn (info));
    g_printf ("device type:%s\n", gupnp_device_info_get_device_type (info));

    SoupURI *soup_uri = gupnp_device_info_get_url_base (info);

    g_assert (soup_uri != NULL);

    g_printf ("soup uri scheme:%s\n", soup_uri_get_scheme (soup_uri));
    g_printf ("soup uri user:%s\n", soup_uri_get_user (soup_uri));
    g_printf ("soup uri password:%s\n", soup_uri_get_password (soup_uri));
    g_printf ("soup uri host:%s\n", soup_uri_get_host (soup_uri));
    g_printf ("soup uri port:%d\n", soup_uri_get_port (soup_uri));
    g_printf ("soup uri path:%s\n", soup_uri_get_path (soup_uri));
    g_printf ("soup uri query:%s\n", soup_uri_get_query (soup_uri));
    g_printf ("soup uri fragment:%s\n", soup_uri_get_fragment (soup_uri));


    char *friendly_name = gupnp_device_info_get_friendly_name (info);
    g_printf ("friendly name:%s\n", friendly_name);
    g_free (friendly_name);

    char *manufacturer = gupnp_device_info_get_manufacturer (info);
    g_printf ("manufacturer:%s\n", manufacturer);
    g_free (manufacturer);

    char *manufacturer_url = gupnp_device_info_get_manufacturer_url (info);
    g_printf ("manufacturer url:%s\n", manufacturer_url);
    g_free (manufacturer_url);

    char *model_desc = gupnp_device_info_get_model_description (info);
    g_printf ("model description:%s\n", model_desc);
    g_free (model_desc);

    char *model_name = gupnp_device_info_get_model_name (info);
    g_printf ("model name:%s\n", model_name);
    g_free (model_name);

    char *model_number = gupnp_device_info_get_model_number (info);
    g_printf ("model number:%s\n", model_number);
    g_free (model_number);

    char *model_url = gupnp_device_info_get_model_url (info);
    g_printf ("model url:%s\n", model_url);
    g_free (model_url);

    char *serial_number = gupnp_device_info_get_serial_number (info);
    g_printf ("serial number:%s\n", serial_number);
    g_free (serial_number);

    char *presentation_url = gupnp_device_info_get_presentation_url (info);
    g_printf ("presentation url:%s\n", presentation_url);
    g_free (presentation_url);

    char *upc = gupnp_device_info_get_upc (info);
    g_printf ("upc:%s\n", upc);
    g_free (upc);

    int depth;
    int width;
    int height;
    char *icon_url;
    icon_url = gupnp_device_info_get_icon_url (info, NULL, -1, -1, -1, FALSE,
                                               NULL, &depth, &width, &height);
    if (icon_url != NULL)
    {
        g_printf ("icon url:%s\n", icon_url);
        g_printf ("icon url depth:%d\n", depth);
        g_printf ("icon url width:%d\n", width);
        g_printf ("icon url height:%d\n", height);
    }
    g_free(icon_url);

    GList *dlna_cap = gupnp_device_info_list_dlna_capabilities (info);
    if (dlna_cap != NULL)
    {
        int i;
        for (i = 0; i < g_list_length (dlna_cap); i++)
        {
            const char *dlna_cap_elem = g_list_nth_data (dlna_cap, i);
            char *desc_value = gupnp_device_info_get_description_value (info, dlna_cap_elem); 
            g_printf ("%d th dlna cap:%s\n", i, dlna_cap_elem);
            if (desc_value != NULL)
            {
                g_printf ("cap vlaue:%s\n", desc_value);
            }
            g_free (desc_value);
        }
    }
    g_list_free (dlna_cap);


    GList *device_list = gupnp_device_info_list_devices (info);
    if (device_list != NULL)
    {
        int j;
        for (j = 0; j < g_list_length (device_list); j++)
        {
            GUPnPDeviceInfo *dev_info = (GUPnPDeviceInfo *) g_list_nth_data (device_list, j);
            g_assert (dev_info != NULL);
            g_printf ("direct child device location:%s\n", gupnp_device_info_get_location (dev_info));
            g_object_unref (dev_info);
        }
    }
    g_list_free (device_list);

    GList *device_types = gupnp_device_info_list_device_types (info);
    if (device_types != NULL)
    {
        int k;
        for (k = 0; k < g_list_length (device_types); k++)
        {
            /*
            const char *type = (const char *) g_list_nth_data (device_types, k);
            if (type != NULL)
            {
                g_printf ("direct child device type:%s\n", type);
                GUPnPDeviceInfo *child_info = gupnp_device_info_get_device (info, type);
                if (child_info != NULL)
                {
                    g_printf ("get child by type, device location:%s\n", gupnp_device_info_get_location (child_info));
                }
                g_object_unref (child_info);
            }
            */
            char *type = (char *) g_list_nth_data (device_types, k);
            g_printf ("device type:%s\n", type);
            g_free (type);
        }
    }
    g_list_free (device_types);
    
    GList *service_list = gupnp_device_info_list_services (info);
    if (service_list != NULL)
    {
        int m;
        for (m = 0; m < g_list_length (service_list); ++m)
        {
            GUPnPServiceInfo *service_info = (GUPnPServiceInfo *) g_list_nth_data (service_list, m);

            //print_service_info (service_info);
            g_printf ("%d th service:%s\n", m, gupnp_service_info_get_udn (service_info));
            g_object_unref (service_info);
        }
    }
    g_list_free (service_list);

    GList *service_types = gupnp_device_info_list_service_types (info);
    if (service_types != NULL)
    {
        int n;
        for (n = 0; n < g_list_length (service_types); n++)
        {
            //const char *type = (const char *) g_list_nth_data (device_types, n);
            char *type = (char *) g_list_nth_data (service_types, n);
            g_printf ("device service type:%s\n", type);
            g_free (type);
            /*
            if (type != NULL)
            {
                g_printf ("direct child service type:%s\n", type);
                GUPnPServiceInfo *child_info = gupnp_device_info_get_service (info, type);
                if (child_info != NULL)
                {
                    g_printf ("get child by type, service udn:%s\n", gupnp_service_info_get_udn (child_info));
                }
                g_object_unref (child_info);
            }
            */
        }
    }
    g_list_free (service_types);
}

void 
on_device_proxy_available (GUPnPControlPoint *control_point,
                           GUPnPDeviceProxy *proxy,
                           gpointer user_data)
{
    char *target = g_strdup (user_data);
    g_warning ("on device proxy available target:%s\n", target);
    g_free (target);
    // print_device_info (proxy);
}

void 
on_device_proxy_unavailable (GUPnPControlPoint *control_point,
                             GUPnPDeviceProxy *proxy,
                             gpointer user_data)
{
    char *target = g_strdup (user_data);
    g_warning ("on device proxy unavailable target:%s\n", target);
    g_free (target);
}

void
on_service_proxy_available (GUPnPControlPoint *control_point,
                            GUPnPServiceProxy *proxy,
                            gpointer user_data)
{
    char *target = g_strdup (user_data);
//    g_warning ("on service proxy available target:%s\n", target);
    g_free (target);

    //print_service_info (GUPNP_SERVICE_INFO (proxy));
    GUPnPServiceInfo *service_info = (GUPnPServiceInfo *) proxy;
    g_assert (service_info != NULL);

    const char * service_type = gupnp_service_info_get_service_type (service_info);

    if (g_strrstr (service_type, "ContentDirectory") != NULL)
    {
        g_printf ("content directory:%s\n", service_type);
        //print_service_info (GUPNP_SERVICE_INFO (proxy));

        test_content_directory (proxy);

    } else if (g_strrstr (service_type, "ConnectionManager") != NULL) {
       // g_printf ("connection manager:%s\n", service_type);
        
    } else {
        //g_printf ("service type:%s\n", service_type);

    }
}

void
on_service_proxy_unavailable (GUPnPControlPoint *control_point,
                              GUPnPServiceProxy *proxy,
                              gpointer user_data)
{
    char *target = g_strdup (user_data);
    g_warning ("on service proxy unavailable target:%s\n", target);
    g_free (target);
}

void 
device_proxy_available_cb (GUPnPControlPoint *control_point,
                             GUPnPDeviceProxy *proxy,
                             gpointer user_data)
{
    g_warning ("%s\n", "device proxy available cb");
    // print_device_info (proxy);

    GUPnPDeviceInfo *info = GUPNP_DEVICE_INFO (proxy);
    g_assert (info != NULL);
    const char *device_type = gupnp_device_info_get_device_type (info);

    if (g_strrstr (device_type, "MediaServer") == NULL)
    {
        g_warning ("currently ignore device which not a media server\n");

    } else {

        GUPnPContext *ctx = gupnp_control_point_get_context (control_point);
        g_assert (ctx != NULL);

        // GUPnPControlPoint *d_cp = dlna_cp_init (ctx, device_type);
        GUPnPControlPoint *d_cp = gupnp_control_point_new (ctx, device_type);
        g_assert (d_cp != NULL);

        g_signal_connect (d_cp,
                         "device-proxy-available",
                         G_CALLBACK (on_device_proxy_available),
                         (gpointer) device_type);

        g_signal_connect (d_cp, 
                         "device-proxy-unavailable",
                         G_CALLBACK (on_device_proxy_unavailable),
                         (gpointer) device_type);

        g_signal_connect (d_cp, 
                         "service-proxy-available",
                         G_CALLBACK (on_service_proxy_available),
                         (gpointer) device_type);

        g_signal_connect (d_cp,
                         "service-proxy-unavailable",
                         G_CALLBACK (on_service_proxy_unavailable),
                         (gpointer) device_type);

        gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (d_cp), TRUE);

        gupnp_context_manager_manage_control_point (context_manager, d_cp);

        GList *service_type_list = gupnp_device_info_list_service_types (info);
        int i;
        for (i = 0; i < g_list_length (service_type_list); i++)
        {
            char *type = (char *) g_list_nth_data (service_type_list, i);
            GUPnPControlPoint *s_cp = gupnp_control_point_new (ctx, type);
            g_assert (s_cp != NULL);

            g_signal_connect (s_cp,
                             "device-proxy-available",
                             G_CALLBACK (on_device_proxy_available),
                             type);

            g_signal_connect (s_cp, 
                             "device-proxy-unavailable",
                             G_CALLBACK (on_device_proxy_unavailable),
                             type);

            g_signal_connect (s_cp, 
                             "service-proxy-available",
                             G_CALLBACK (on_service_proxy_available),
                             type);

            g_signal_connect (s_cp,
                             "service-proxy-unavailable",
                             G_CALLBACK (on_service_proxy_unavailable),
                             type);

            gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (s_cp), TRUE);\
            gupnp_context_manager_manage_control_point (context_manager, s_cp);
        }
        g_list_free (service_type_list);

    }

}

void
device_proxy_unavailable_cb (GUPnPControlPoint *control_point,
                             GUPnPDeviceProxy *proxy,
                             gpointer user_data)
{
    g_warning ("%s\n", "device proxy unavailable cb");
}

void 
service_proxy_available_cb (GUPnPControlPoint *control_point,
                            GUPnPServiceProxy *proxy,
                            gpointer user_data)
{
    g_warning ("%s\n", "service proxy available cb");
}


void 
service_proxy_unavailable_cb (GUPnPControlPoint *control_point, 
                              GUPnPServiceProxy *proxy,
                              gpointer user_data)
{
    g_warning ("%s\n", "service proxy unavailable cb");
}


GUPnPControlPoint *
dlna_cp_init (GUPnPContext *context, const char *target)
{
    GUPnPControlPoint *cp;
    cp = gupnp_control_point_new (context, target);
    g_assert (cp != NULL);

    g_signal_connect (cp,
                      "device-proxy-available",
                      G_CALLBACK (device_proxy_available_cb),
                      NULL);

    g_signal_connect (cp, 
                      "device-proxy-unavailable",
                      G_CALLBACK (device_proxy_unavailable_cb),
                      NULL);

    g_signal_connect (cp, 
                      "service-proxy-available",
                      G_CALLBACK (service_proxy_available_cb),
                      NULL);

    g_signal_connect (cp,
                      "service-proxy-unavailable",
                      G_CALLBACK (service_proxy_unavailable_cb),
                      NULL);

    return cp;
}

void 
dlna_cp_deinit (GUPnPContext *context)
{
    g_warning ("%s\n", "dlan cp deinit");
}
