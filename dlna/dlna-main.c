#include "dlna-cp.h"

static GMainLoop *mainloop;

static void
on_context_available(GUPnPContextManager *context_manager,
                     GUPnPContext *context,
                     gpointer user_data)
{
    g_warning ("%s\n", "context available cb");

    GUPnPControlPoint *root_cp;

    root_cp = dlna_cp_init (context, "upnp:rootdevice");
    g_assert (root_cp != NULL);

    GSSDPClient *client = gssdp_resource_browser_get_client (GSSDP_RESOURCE_BROWSER (root_cp));

    if(g_strcmp0(gssdp_client_get_interface (client), "lo") != 0)
    {
        g_printf ("ssdp server id:%s\n", gssdp_client_get_server_id (client));
        g_printf ("ssdp interface:%s\n", gssdp_client_get_interface (client));
        g_printf ("ssdp host ip:%s\n", gssdp_client_get_host_ip (client));
        g_printf ("ssdp network:%s\n", gssdp_client_get_network (client));
        g_printf ("ssdp active:%d\n", gssdp_client_get_active (client));

        gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (root_cp), TRUE);
        gupnp_context_manager_manage_control_point (context_manager, root_cp);
    }

    g_object_unref (root_cp);
}

static void
on_context_unavailable(GUPnPContextManager *context_manager,
                       GUPnPContext *context,
                       gpointer user_data)
{
    g_warning ("%s\n", "context unavailable cb");

}

int 
main (int argc, char **argv)
{
    mainloop = g_main_loop_new (NULL, FALSE);
    context_manager = gupnp_context_manager_create (0);
    g_assert (context_manager != NULL);

    g_signal_connect (context_manager,
                      "context-available",
                      G_CALLBACK (on_context_available),
                      NULL);

    g_signal_connect (context_manager,
                      "context-unavailable",
                      G_CALLBACK (on_context_unavailable),
                      NULL);

    g_main_loop_run (mainloop);

    return 0;
}
