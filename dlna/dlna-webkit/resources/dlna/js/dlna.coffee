#Copyright (c) 2011 ~ 2013 Deepin, Inc.
#              2011 ~ 2013 yilang
#
#Author:      LongWei <yilang2007lw@gmail.com>
#Maintainer:  LongWei <yilang2007lw@gmail.com>
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, see <http://www.gnu.org/licenses/>.

class Container extends Widget
    constructor: (@id)->
        super
        @client = create_element("div", "Client", @element)
        @client_ctl = create_element("div", "ClientCtl", @client)

        @client_desc = create_element("span", "ClientDesc", @client_ctl)
        @client_desc.innerText = "来自小伙伴的分享"

        @scan = create_element("input", "Scan",@client_ctl)
        @scan.setAttribute("type","button")
        @scan.setAttribute("value", "Scan")
        @scan.addEventListener("click", (e) =>
            echo "start scan"
            DCore.Dlna.client_start_discovery()
        )

        @stop_scan = create_element("input", "StopScan", @client_ctl)
        @stop_scan.setAttribute("type","button")
        @stop_scan.setAttribute("value","Stop Scan")
        @stop_scan.addEventListener("click", (e) =>
            echo "stop scan"
            DCore.Dlna.client_stop_discovery()
        )

        @device_list = create_element("div", "DeviceList", @client)

        @server = create_element("div", "Server", @element)
        @server_ctl = create_element("div", "ServerCtl", @server)

        @server_desc = create_element("span", "ServerDesc", @server_ctl)
        @server_desc.innerText = "我给小伙伴们的世界"

        @share = create_element("input","Share", @server_ctl)
        @share.setAttribute("type","button")
        @share.setAttribute("value","Share")
        @share.addEventListener("click", (e) =>
            echo "start share"
            DCore.Dlna.server_start()
        )

        @stop_share = create_element("input","StopShare",@server_ctl)
        @stop_share.setAttribute("type","button")
        @stop_share.setAttribute("value","StopShare")
        @stop_share.addEventListener("click", (e) =>
            echo "stop share"
            DCore.Dlna.server_stop()
        )

        @share_list = create_element("div", "ShareList", @server)

    update_device_list: ->
        echo "update device list"
        @device_list.removeChildAll()
        @devices = DCore.Dlna.client_get_device_list()
        for device in @devices
            device_item = create_element("div", "DeviceItem", @device_list)
            device_item.innerText = DCore.Dlna.client_get_device_friendly_name(device)

    update_share_list: ->
        echo "update share list"

container = new Container("dlna")
document.body.appendChild(container.element)

DCore.signal_connect("device-state", ->
    echo "device state signal"
    container.update_device_list()
)
