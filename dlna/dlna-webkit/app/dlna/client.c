/**
 * Copyright (c) 2011 ~ 2013 Deepin, Inc.
 *               2011 ~ 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "client.h"

static GUPnPContextManager *context_manager = NULL;
static GUPnPControlPoint *root_cp = NULL;
static GHashTable *device_dict = NULL;

static void device_proxy_available_cb (GUPnPControlPoint *control_point,
                                       GUPnPDeviceProxy *proxy,
                                       gpointer user_data);

static void device_proxy_unavailable_cb (GUPnPControlPoint *control_point,
                                         GUPnPDeviceProxy *proxy,
                                         gpointer user_data);

static void service_proxy_available_cb (GUPnPControlPoint *control_point,
                                        GUPnPServiceProxy *proxy,
                                        gpointer user_data);

static void service_proxy_unavailable_cb (GUPnPControlPoint *control_point, 
                                          GUPnPServiceProxy *proxy,
                                          gpointer user_data);
static void
on_context_available (GUPnPContextManager *context_manager,
                      GUPnPContext *context,
                      gpointer user_data)
{
    g_debug ("on context available");

    if (root_cp == NULL) {
        root_cp = gupnp_control_point_new (context, "upnp:rootdevice");
        //root_cp = gupnp_control_point_new (context, "ssdp:all");
        g_signal_connect (root_cp,
                          "device-proxy-available",
                          G_CALLBACK (device_proxy_available_cb),
                          NULL);
        g_signal_connect (root_cp, 
                          "device-proxy-unavailable",
                          G_CALLBACK (device_proxy_unavailable_cb),
                          NULL);
        g_signal_connect (root_cp, 
                          "service-proxy-available",
                          G_CALLBACK (service_proxy_available_cb),
                          NULL);
        g_signal_connect (root_cp,
                          "service-proxy-unavailable",
                          G_CALLBACK (service_proxy_unavailable_cb),
                          NULL);
    } else {
        g_debug ("on context available:alread create root cp");
    }

    GSSDPClient *client = gssdp_resource_browser_get_client (GSSDP_RESOURCE_BROWSER (root_cp));
    if (client == NULL) {
        g_warning ("client start discovery:get client from root cp failed\n");
    }

    if (g_strcmp0 ("l0", gssdp_client_get_interface (client)) == 0) {
        g_warning ("client start discovery:network unavilable\n");
    }

    gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (root_cp), TRUE);
    gupnp_context_manager_manage_control_point (context_manager, root_cp);
}

static void
on_context_unavailable (GUPnPContextManager *context_manager,
                        GUPnPContext *context,
                        gpointer user_data)
{
    g_debug ("on context unavailable");
}

void init_client ()
{
    context_manager = gupnp_context_manager_create (0);
    if (context_manager == NULL) {
        g_warning ("init client:create context manager failed\n");
        return ;
    }

    device_dict = g_hash_table_new (g_str_hash, g_str_equal);
    if (device_dict == NULL) {
        g_warning ("init client:init device dict failed\n");
        return ;
    }

    g_signal_connect (context_manager,
                      "context-available",
                      G_CALLBACK (on_context_available),
                      NULL);

    g_signal_connect (context_manager,
                      "context-unavailable",
                      G_CALLBACK (on_context_unavailable),
                      NULL);
}

static void 
device_proxy_available_cb (GUPnPControlPoint *control_point,
                           GUPnPDeviceProxy *proxy,
                           gpointer user_data)
{
    g_debug ("device proxy available");

    GUPnPDeviceInfo *device_info = GUPNP_DEVICE_INFO (proxy);

    const char *device_type = gupnp_device_info_get_device_type (device_info);
    const char *udn = gupnp_device_info_get_udn (device_info);

    g_hash_table_insert (device_dict, g_strdup (udn), device_info);

    js_post_message_simply("device-state", NULL);

    if (g_strrstr (device_type, "InternetGatewayDevice") != NULL) {
        g_debug ("device proxy available:InternetGatewayDevice");

    } else if (g_strrstr (device_type, "MediaServer") != NULL) {
        g_debug ("device proxy available:MediaServer");

    } else if (g_strrstr (device_type, "MediaRender") != NULL) {
        g_debug ("device proxy available:MediaRender");

    } else {
        g_debug ("device proxy available:other device");
    }
}

static void
device_proxy_unavailable_cb (GUPnPControlPoint *control_point,
                             GUPnPDeviceProxy *proxy,
                             gpointer user_data)
{
    g_debug ("device proxy unavailable cb");

    GUPnPDeviceInfo *device_info = GUPNP_DEVICE_INFO (proxy);
    const char *udn = gupnp_device_info_get_udn (device_info);

    g_hash_table_remove (device_dict, udn);
    js_post_message_simply ("device-state", NULL);
}

static void 
service_proxy_available_cb (GUPnPControlPoint *control_point,
                            GUPnPServiceProxy *proxy,
                            gpointer user_data)
{
    g_debug ("service proxy available cb");

    GUPnPServiceInfo *service_info = (GUPnPServiceInfo *) proxy;
    if (service_info == NULL) {
        g_warning ("service proxy available:service info is NULL\n");
        return ;
    }

    const char * service_type = gupnp_service_info_get_service_type (service_info);

    if (g_strrstr (service_type, "ContentDirectory") != NULL) {
        g_debug ("service proxy available:ContentDirectory");

    } else if (g_strrstr (service_type, "ConnectionManager") != NULL) {
        g_debug ("service proxy available:ConnectionManager");
        
    } else {
        g_debug ("service proxy available:%s\n", service_type);
    }
}

static void 
service_proxy_unavailable_cb (GUPnPControlPoint *control_point, 
                              GUPnPServiceProxy *proxy,
                              gpointer user_data)
{
    g_debug ("service proxy unavailable cb");
}

JS_EXPORT_API 
gboolean dlna_client_start_discovery ()
{
    gboolean ret = FALSE;

    g_debug ("client start discovery");
    if (context_manager == NULL) {
        g_warning ("client start discovery:context manager is NULL\n");
        return ret;
    }

    if (root_cp == NULL) {
        g_warning ("client start discovery:root cp is NULL\n");
        return ret;
    }

    GSSDPClient *client = gssdp_resource_browser_get_client (GSSDP_RESOURCE_BROWSER (root_cp));
    if (client == NULL) {
        g_warning ("client start discovery:get client from root cp failed\n");
        return ret;
    }

    if (g_strcmp0 ("l0", gssdp_client_get_interface (client)) == 0) {
        g_warning ("client start discovery:network unavilable\n");
    }

    gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (root_cp), TRUE);
    gupnp_context_manager_manage_control_point (context_manager, root_cp);
    ret = TRUE;

    return ret;
}

JS_EXPORT_API 
gboolean dlna_client_stop_discovery ()
{
    gboolean ret = FALSE;

    if (root_cp == NULL) {
        g_warning ("client stop discovery:root cp is NULL\n");
        return ret;
    }

    gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (root_cp), FALSE);
    ret = TRUE;

    return ret;
}

JS_EXPORT_API 
JSObjectRef dlna_client_get_device_list ()
{
    JSObjectRef array = json_array_create ();

    GList *keys = g_hash_table_get_keys (device_dict);
    int i;

    for (i = 0; i < g_list_length (keys); i++) {
        json_array_insert (array, i, jsvalue_from_cstr (get_global_context (), g_strdup (g_list_nth_data (keys, i))));
    }

    g_list_free (keys);

    return array;
}

JS_EXPORT_API 
gchar * dlna_client_get_device_friendly_name (const gchar *device)
{
    gchar *name = NULL;
    GUPnPDeviceInfo *device_info = NULL;

    device_info = g_hash_table_lookup (device_dict, device);
    if (device_info == NULL) {
        g_warning ("client get device friendly name:lookup device info failed\n");
        return name;
    }

    name = gupnp_device_info_get_friendly_name (device_info);

    return name;
}

JS_EXPORT_API 
gboolean dlna_client_play_item (const gchar *item)
{
    return FALSE;
}
