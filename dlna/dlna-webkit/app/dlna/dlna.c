/**
 * Copyright (c) 2011 ~ 2013 Deepin, Inc.
 *               2011 ~ 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include "jsextension.h"
#include "dwebview.h"
#include "i18n.h"
#include "utils.h"
#include "client.h"
#include "server.h"

#define DLNA_HTML_PATH "file://"RESOURCE_DIR"/dlna/index.html"

static GtkWidget *container = NULL;

JS_EXPORT_API
gchar *dlna_hello ()
{
    return g_strdup ("Hello World");
}

int main(int argc, char **argv)
{
    init_i18n ();
    gtk_init (&argc, &argv);

    container = create_web_container (FALSE, TRUE);

    gtk_window_set_decorated (GTK_WINDOW (container), FALSE);
    gtk_window_set_skip_taskbar_hint (GTK_WINDOW (container), TRUE);
    gtk_window_set_skip_pager_hint (GTK_WINDOW (container), TRUE);
    gtk_window_fullscreen (GTK_WINDOW (container));

    GtkWidget *webview = d_webview_new_with_uri (DLNA_HTML_PATH);
    gtk_container_add (GTK_CONTAINER (container), GTK_WIDGET (webview));
    gtk_widget_realize (container);

    GdkWindow *gdkwindow = gtk_widget_get_window (container);
    GdkRGBA rgba = { 0, 0, 0, 0.0 };
    gdk_window_set_background_rgba (gdkwindow, &rgba);
    gdk_window_set_cursor (gdkwindow, gdk_cursor_new (GDK_LEFT_PTR));

    gtk_widget_show_all (container);

    init_client ();
    init_server ();
    monitor_resource_file ("dlna", webview);
    gtk_main ();

    return 0;
}
