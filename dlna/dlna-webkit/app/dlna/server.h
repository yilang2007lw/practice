/**
 * Copyright (c) 2011 ~ 2013 Deepin, Inc.
 *               2011 ~ 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __DLNA_SERVER_H
#define __DLNA_SERVER_H

#include <glib.h>
#include <gio/gio.h>
#include <rygel-server.h>
#include <rygel-core.h>
#include "jsextension.h"

void init_server ();

JS_EXPORT_API gboolean dlna_server_start ();

JS_EXPORT_API gboolean dlna_server_stop ();

JS_EXPORT_API gboolean dlna_server_set_interface (const gchar *interface);

JS_EXPORT_API JSObjectRef dlna_server_get_interfaces ();

JS_EXPORT_API JSObjectRef dlna_server_get_containers ();

JS_EXPORT_API gboolean dlna_server_add_container (const gchar *container);

JS_EXPORT_API gboolean dlna_server_remove_container (const gchar *container);

#endif
