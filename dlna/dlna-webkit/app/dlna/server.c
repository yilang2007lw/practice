/**
 * Copyright (c) 2011 ~ 2013 Deepin, Inc.
 *               2011 ~ 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "server.h"

static RygelMediaServer *server = NULL;
static RygelSimpleContainer *root_container = NULL;
static gboolean started = FALSE;

void add_media_item (const gchar *path, RygelSimpleContainer *parent)
{
    g_debug ("add media item:path->%s\n", path);

    GFile *file;
    GFileInfo *info;
    const gchar *content_type;
    gchar *uri, *id;
    RygelMediaItem *item = NULL;
    GError *error = NULL;

    if (parent == NULL) {
        g_warning ("add media item:parent container is NULL\n");
        return ;
    }

    file = g_file_new_for_path (path);
    if (file == NULL) {
        g_warning ("add media item:file\n");
    }

    info = g_file_query_info (file, "standard::", G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS, NULL, &error);
    if (error != NULL) {
        g_warning ("add media item:query info %s\n", error->message);
        g_error_free (error);
        return ;
    }

    content_type = g_file_info_get_content_type (info);
    uri = g_file_get_uri (file);
    g_object_unref (file);

    id = g_strdup (path);

    if (g_str_has_prefix (content_type, "audio/")) {
        g_debug ("add media item:audio path->%s\n", path);
        item = rygel_audio_item_new (id,
                                     (RygelMediaContainer *)parent,
                                     id,
                                     RYGEL_AUDIO_ITEM_UPNP_CLASS);

    } else if (g_str_has_prefix (content_type, "video/")) {
        g_debug ("add media item:video path->%s\n", path);
        item = rygel_video_item_new (id,
                                     (RygelMediaContainer *)parent,
                                     id,
                                     RYGEL_VIDEO_ITEM_UPNP_CLASS);

    } else if (g_str_has_prefix (content_type, "image/")) {
        g_debug ("add media item:image path->%s\n", path);
        item = rygel_image_item_new (id,
                                     (RygelMediaContainer *)parent,
                                     id,
                                     RYGEL_IMAGE_ITEM_UPNP_CLASS);
    }
    g_free (id);

    if (item != NULL) {
        rygel_media_item_set_mime_type (RYGEL_MEDIA_ITEM (item), content_type);
        rygel_media_item_add_uri (RYGEL_MEDIA_ITEM (item), uri);
        rygel_simple_container_add_child_item (parent, item);
    }
}

void walk_directory (const gchar *root, RygelSimpleContainer *parent)
{
    g_debug ("walk directory:path->%s\n", root);

    GFile *source_dir = NULL;
    GFileInfo *info = NULL;
    GError *error = NULL;

    source_dir = g_file_new_for_path (root);
    info = g_file_query_info (source_dir, "standard::", G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS, NULL, &error);
    if (error != NULL) {
        g_warning ("walk directory:query info %s\n", error->message);
        g_error_free (error);
        return ;
    }
    error = NULL;

    if (g_file_info_get_file_type (info) == G_FILE_TYPE_REGULAR) {
        if (parent != NULL) {
            add_media_item (root, parent);
        }
    } else if (g_file_info_get_file_type (info) == G_FILE_TYPE_DIRECTORY) {
        RygelSimpleContainer *cur_container = NULL;

        if (parent == NULL) {
            g_warning ("walk directory:parent container is NULL\n");
        } else {
            cur_container = rygel_simple_container_new (root, RYGEL_MEDIA_CONTAINER (parent), root);
        }

        GFileEnumerator *enumerator = g_file_enumerate_children (source_dir, 
                                                                "standard::type", 
                                                                G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS, 
                                                                NULL, 
                                                                &error);
        if (error != NULL) {
            g_warning ("walk directory:enumerate children %s\n", error->message);
            g_error_free (error);
        }
        error = NULL;

        GFileInfo *child_info = g_file_enumerator_next_file (enumerator, NULL, &error);
        if (error != NULL) {
            g_warning ("walk directory:enumerate next file%s\n", error->message);
            g_error_free (error);
        }
        error = NULL;

        while (child_info != NULL) {
            gchar *path = g_strdup_printf ("%s/%s", root, g_file_info_get_name (child_info));

            walk_directory (path, cur_container);

            g_free (path);
            child_info = g_file_enumerator_next_file (enumerator, NULL, &error);
            if (error != NULL) {
                g_warning ("walk directory:enumerate next in while %s\n", error->message);
                g_error_free (error);
            }
            error = NULL;
        }
        rygel_simple_container_add_child_container (parent, cur_container);

        //g_object_unref (child_info);
        g_object_unref (enumerator);

    } else {
        g_debug ("walk directory:current not support\n");
        return ;
    }

    g_object_unref (info);
    g_object_unref (source_dir);
}

void init_server ()
{
    GError *error = NULL;

    rygel_media_engine_init (&error);
    if (error != NULL) {
        g_warning ("init server:init media engine failed\n");
        g_error_free (error);
        return;
    }

    root_container = rygel_simple_container_new_root ("yilang");
    if (root_container == NULL) {
        g_warning ("init server:root container is NULL\n");
        return;
    }

    walk_directory ("/home/longwei/Media", root_container);

    server = rygel_media_server_new ("yilang", RYGEL_MEDIA_CONTAINER (root_container), RYGEL_PLUGIN_CAPABILITIES_NONE);
    if (server == NULL) {
        g_warning ("init server:create server failed\n");
        return ;
    }
}

JS_EXPORT_API 
gboolean dlna_server_start ()
{
    if (started) {
        return TRUE;
    }

    dlna_server_set_interface ("wlan0");
    started = TRUE;

    return started;
}

JS_EXPORT_API 
gboolean dlna_server_stop ()
{
    g_warning ("server stop:currently not implement\n");
    return FALSE;
}

JS_EXPORT_API 
gboolean dlna_server_set_interface (const gchar *interface)
{
    gboolean ret = FALSE;

    if (server == NULL) {
        g_warning ("server set interface\n");
        return ret;
    }

    rygel_media_device_add_interface (RYGEL_MEDIA_DEVICE (server), interface);
    ret = TRUE;

    return ret;
}

JS_EXPORT_API 
JSObjectRef dlna_server_get_interfaces ()
{
    JSObjectRef array = json_array_create ();

    GList *interfaces = rygel_media_device_get_interfaces (RYGEL_MEDIA_DEVICE (server));
    int i;

    for (i = 0; i < g_list_length (interfaces); i++) {
        json_array_insert (array, i, jsvalue_from_cstr (get_global_context (),
                                        g_strdup (g_list_nth_data (interfaces, i))));
    }

    return array;
}

JS_EXPORT_API 
JSObjectRef dlna_server_get_containers ()
{
    return NULL;
}

JS_EXPORT_API 
gboolean dlna_server_add_container (const gchar *container)
{
    return FALSE;
}

JS_EXPORT_API 
gboolean dlna_server_remove_container (const gchar *container)
{
    return FALSE;
}
