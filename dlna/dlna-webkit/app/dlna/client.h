/**
 * Copyright (c) 2011 ~ 2013 Deepin, Inc.
 *               2011 ~ 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __DLNA_CLIENT_H
#define __DLNA_CLIENT_H

#include <glib.h>
#include <libgssdp/gssdp.h>
#include <libgupnp/gupnp.h>
#include <libgupnp-av/gupnp-av.h>
#include "jsextension.h"

void init_client ();

JS_EXPORT_API gboolean dlna_client_start_discovery ();

JS_EXPORT_API gboolean dlna_client_stop_discovery ();

JS_EXPORT_API JSObjectRef dlna_client_get_device_list ();

JS_EXPORT_API gchar * dlna_client_get_device_friendly_name (const gchar *device);

JS_EXPORT_API gboolean dlna_client_play_item (const gchar *item);

#endif
