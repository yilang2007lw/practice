#ifndef DLNA_CP_H
#define DLNA_CP_H

#include <libgupnp/gupnp.h>
#include <glib.h>

GUPnPContextManager *context_manager;

GUPnPControlPoint* dlna_cp_init (GUPnPContext *context, const char *target);

void dlna_cp_deinit (GUPnPContext *context);

#endif
