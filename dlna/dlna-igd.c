#include <libgupnp/gupnp.h>
#include <glib.h>

static GUPnPContextManager *context_manager;

void 
on_device_proxy_available (GUPnPControlPoint *control_point,
                           GUPnPDeviceProxy *proxy,
                           gpointer user_data)
{
    const char* udn = (const char *) user_data;
    g_assert (udn != NULL);

    g_printf ("on device proxy available for:%s\n", udn);

    GUPnPDeviceInfo *d_info = GUPNP_DEVICE_INFO (proxy);

    g_printf ("device udn:%s\n", gupnp_device_info_get_udn (d_info));
    g_printf ("device location:%s\n", gupnp_device_info_get_location (d_info));
}

void 
on_device_proxy_unavailable (GUPnPControlPoint *control_point,
                           GUPnPDeviceProxy *proxy,
                           gpointer user_data)
{
    const char* udn = (const char *) user_data;
    g_assert (udn != NULL);

    g_printf ("on device proxy unavailable for:%s\n", udn);
}

void 
on_service_proxy_available (GUPnPControlPoint *control_point,
                           GUPnPServiceProxy *proxy,
                           gpointer user_data)
{
    const char* udn = (const char *) user_data;
    g_assert (udn != NULL);

    g_printf ("on service proxy available for:%s\n", udn);
}

void 
on_service_proxy_unavailable (GUPnPControlPoint *control_point,
                           GUPnPServiceProxy *proxy,
                           gpointer user_data)
{
    const char* udn = (const char *) user_data;
    g_assert (udn != NULL);

    g_printf ("on service proxy unavailable:%s\n", udn);
}

void 
device_proxy_available_cb (GUPnPControlPoint *control_point,
                           GUPnPDeviceProxy *proxy,
                           gpointer user_data)
{
    GUPnPDeviceInfo *device_info = GUPNP_DEVICE_INFO (proxy);

    const char *device_type = gupnp_device_info_get_device_type (device_info);

    if (g_str_has_prefix (device_type, "urn:schemas-upnp-org:device:InternetGatewayDevice"))
    {
        g_printf ("%s\n", "device proxy available cb");

        g_printf ("device type:%s\n", gupnp_device_info_get_device_type (device_info));

        const char *udn = gupnp_device_info_get_udn (device_info);
        g_assert (udn != NULL);

        GUPnPContext *ctx = gupnp_control_point_get_context (control_point);
        g_assert (ctx != NULL);

        GUPnPControlPoint *d_cp = gupnp_control_point_new (ctx, device_type);
        g_assert (d_cp != NULL);

        g_signal_connect (d_cp,
                         "device-proxy-available",
                         G_CALLBACK (on_device_proxy_available),
                         (gpointer) udn);

        g_signal_connect (d_cp, 
                         "device-proxy-unavailable",
                         G_CALLBACK (on_device_proxy_unavailable),
                         (gpointer) udn);

        g_signal_connect (d_cp, 
                         "service-proxy-available",
                         G_CALLBACK (on_service_proxy_available),
                         (gpointer) udn);

        g_signal_connect (d_cp,
                         "service-proxy-unavailable",
                         G_CALLBACK (on_service_proxy_unavailable),
                         (gpointer) udn);

        gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (d_cp), TRUE);

        gupnp_context_manager_manage_control_point (context_manager, d_cp);

        g_printf ("\nlist device for %s:\n", udn);
        GList *device_list = gupnp_device_info_list_devices (device_info);
        if (device_list != NULL)
        {
            int j;
            for (j = 0; j < g_list_length (device_list); j++)
            {
                GUPnPDeviceInfo *dev_info = (GUPnPDeviceInfo *) g_list_nth_data (device_list, j);
                g_assert (dev_info != NULL);
                g_printf ("direct child device location:%s\n", gupnp_device_info_get_location (dev_info));
                g_object_unref (dev_info);
            }
        }
        g_list_free (device_list);

        g_printf ("\nlist service for %s:\n", udn);
        GList *service_list = gupnp_device_info_list_services (device_info);
        if (service_list != NULL)
        {
            int m;
            for (m = 0; m < g_list_length (service_list); ++m)
            {
                GUPnPServiceInfo *service_info =  GUPNP_SERVICE_INFO (g_list_nth_data (service_list, m));
                g_printf ("%d th service:%s\n", m, gupnp_service_info_get_udn (service_info));
                g_object_unref (service_info);
            }
        }
        g_list_free (service_list);

        g_object_unref (d_cp);
    }
}

void
device_proxy_unavailable_cb (GUPnPControlPoint *control_point,
                             GUPnPDeviceProxy *proxy,
                             gpointer user_data)
{
    g_printf ("%s\n", "device proxy unavailable cb");
}

void 
service_proxy_available_cb (GUPnPControlPoint *control_point,
                            GUPnPServiceProxy *proxy,
                            gpointer user_data)
{
    g_printf ("%s\n", "service proxy available cb");
}


void 
service_proxy_unavailable_cb (GUPnPControlPoint *control_point, 
                              GUPnPServiceProxy *proxy,
                              gpointer user_data)
{
    g_printf ("%s\n", "service proxy unavailable cb");
}

static void
on_context_available(GUPnPContextManager *context_manager,
                     GUPnPContext *context,
                     gpointer user_data)
{
    g_printf ("%s\n", "context available cb");

    GUPnPControlPoint *root_cp = gupnp_control_point_new (context, "upnp:rootdevice");
    g_assert (root_cp != NULL);

    GSSDPClient *client = gssdp_resource_browser_get_client (GSSDP_RESOURCE_BROWSER (root_cp));

    if(g_strcmp0(gssdp_client_get_interface (client), "lo") != 0)
    {

        g_signal_connect (root_cp,
                          "device-proxy-available",
                          G_CALLBACK (device_proxy_available_cb),
                          NULL);

        g_signal_connect (root_cp, 
                          "device-proxy-unavailable",
                          G_CALLBACK (device_proxy_unavailable_cb),
                          NULL);

        g_signal_connect (root_cp, 
                          "service-proxy-available",
                          G_CALLBACK (service_proxy_available_cb),
                          NULL);

        g_signal_connect (root_cp,
                          "service-proxy-unavailable",
                          G_CALLBACK (service_proxy_unavailable_cb),
                          NULL);

        gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (root_cp), TRUE);
        gupnp_context_manager_manage_control_point (context_manager, root_cp);
    }

    g_object_unref (root_cp);
}

static void
on_context_unavailable(GUPnPContextManager *context_manager,
                       GUPnPContext *context,
                       gpointer user_data)
{
    g_printf ("%s\n", "context unavailable cb");
}

int 
main (int argc, char **argv)
{
    GMainLoop *mainloop = g_main_loop_new (NULL, FALSE);

    context_manager = gupnp_context_manager_new (NULL, 0);
    g_assert (context_manager != NULL);

    g_signal_connect (context_manager,
                      "context-available",
                      G_CALLBACK (on_context_available),
                      NULL);

    g_signal_connect (context_manager,
                      "context-unavailable",
                      G_CALLBACK (on_context_unavailable),
                      NULL);

    g_main_loop_run (mainloop);

    return 0;
}
