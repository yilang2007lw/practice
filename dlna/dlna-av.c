#include <libgupnp/gupnp.h>
#include <libgupnp-av/gupnp-av.h>
#include <libgssdp/gssdp.h>
#include <glib.h>
#include <libxml/tree.h>

static GUPnPContextManager *context_manager;

void cds_browse_content (GUPnPServiceProxy *proxy, const char *id);

void 
print_list (gpointer data,
            gpointer user_data)
{
    char *item = (char *) data;
    const char *who = (const char *)user_data;

    g_printf ("%s:%s\n", user_data, data);

    g_free (item);
}

void
test_didl_container (GUPnPDIDLLiteContainer *container)
{
    g_printf ("\ntest didl container\n");

    g_printf ("container searchable:%d\n", gupnp_didl_lite_container_get_searchable (container));
    g_printf ("container child count:%d\n", gupnp_didl_lite_container_get_child_count (container));
    
    int i;
    GList *create_classes = gupnp_didl_lite_container_get_create_classes (container);
    for (i = 0; i < g_list_length (create_classes); i++)
    {
        char *class = (char *) g_list_nth_data (create_classes, i);
        g_printf ("create classes:%s\n", class);
        g_free (class);
    }
    //g_list_foreach (create_classes, print_list, "create classes");
    g_list_free (create_classes);

    
    GList *search_classes = gupnp_didl_lite_container_get_search_classes (container);
    for (i = 0; i < g_list_length (search_classes); i++)
    {
        char *class = (char *) g_list_nth_data (search_classes, i);
        g_printf ("search classes:%s\n", class);
        g_free (class);
    }
    g_list_free (search_classes);

    /*
    gint64 storage_used = gupnp_didl_lite_container_get_storage_used (container);
    g_printf ("container storage used:%"G_GINT64_FORMAT, storage_used);
    */
}

void
test_didl_item (GUPnPDIDLLiteItem *item)
{
    g_printf ("\ntest didl item\n");

    /*
    g_printf ("item ref id:%s\n", gupnp_didl_lite_item_get_ref_id (item));
    g_printf ("item lifetime:%d\n", gupnp_didl_lite_item_get_lifetime (item));
    */

    GUPnPDIDLLiteObject *object = GUPNP_DIDL_LITE_OBJECT (item);

    xmlNode *xml_node = gupnp_didl_lite_object_get_xml_node (object);
    g_assert (xml_node != NULL);

    g_printf ("father\n");
    g_printf ("element type:%d\n", xml_node->type);
    g_printf ("name:%s\n", xml_node->name);    
    g_printf ("content:%s\n", xml_node->content);
    g_printf ("ns:%s\n", g_strdup_printf ("%s%s", xml_node->ns->prefix, xml_node->ns->href));
    g_printf ("attr name:%s\tvalue name:%svalue content:%s\n", xml_node->properties->name,
                xml_node->properties->children->name, xml_node->properties->children->content);
    xmlNode *children = xml_node->children;
    while (children != NULL && children != xml_node->last)
    {
        if (g_strcmp0 (children->name, "title") == 0)
        {
            g_printf ("title\n");
        }
        else if (g_strcmp0 (children->name, "class") == 0) 
        {
            g_printf ("class\n");
        }
        else if (g_strcmp0 (children->name, "date") == 0)
        {
            g_printf ("date\n");
        }
        else if (g_strcmp0 (children->name, "res") == 0)
        {
            g_printf ("res\n");
        }
        else
        {
            ;
        }
        g_printf ("child name:%s\tcontent:%s\n", children->name, children->children->content);
        g_printf ("element type:%d\n", children->type);
        g_printf ("name:%s\n", children->name);
        g_printf ("content:%s\n", children->content);
        g_printf ("ns:%s", g_strdup_printf ("%s%s", children->ns->prefix, children->ns->href));

        children = children->next;
    }
    
    /*
    xmlNsPtr upnp_ns = gupnp_didl_lite_object_get_upnp_namespace (object);
    g_assert (upnp_ns != NULL);
    g_printf ("upnp_ns:%s\n", upnp_ns->href);

    xmlNsPtr dc_ns = gupnp_didl_lite_object_get_dc_namespace (object);
    g_assert (dc_ns != NULL);
    g_printf ("dc_ns:%s\n", dc_ns->prefix);

    xmlNsPtr dlna_ns = gupnp_didl_lite_object_get_dlna_namespace (object);
    g_assert (dlna_ns != NULL);
    g_printf ("dlna namespace:%s\n", dlna_ns->href);
    */
}

void
test_didl_object (GUPnPDIDLLiteObject *object)
{
    g_printf ("\ntest didl object\n");

    /* test xml related in test_didl_item */
//    xmlNode *xml_node = gupnp_didl_lite_object_get_xml_node (object);

    g_printf ("object id:%s\n", gupnp_didl_lite_object_get_id (object));
    g_printf ("object parent id:%s\n", gupnp_didl_lite_object_get_parent_id (object));
    g_printf ("object restricted:%d\n", gupnp_didl_lite_object_get_restricted (object));
    g_printf ("object title:%s\n", gupnp_didl_lite_object_get_title (object));
    g_printf ("object creator:%s\n", gupnp_didl_lite_object_get_creator (object));
    g_printf ("object artist:%s\n", gupnp_didl_lite_object_get_artist (object));

    int i;
    GList *creators = gupnp_didl_lite_object_get_creators (object);
    for (i = 0; i < g_list_length (creators); i++)
    {
        GUPnPDIDLLiteContributor *contributor = (GUPnPDIDLLiteContributor *) g_list_nth_data (creators, i);
        g_assert (contributor != NULL);
        g_printf ("object contributor role:%s\n", gupnp_didl_lite_contributor_get_role (contributor));
        g_printf ("object contributor name:%s\n", gupnp_didl_lite_contributor_get_name (contributor));

        g_object_unref (contributor);
    }
    g_list_free (creators);

    GList *artists = gupnp_didl_lite_object_get_artists (object);
    for (i = 0; i < g_list_length (artists); i++)
    {
        GUPnPDIDLLiteContributor *contributor = (GUPnPDIDLLiteContributor *) g_list_nth_data (artists, i);
        g_assert (contributor != NULL);
        g_printf ("object contributor role:%s\n", gupnp_didl_lite_contributor_get_role (contributor));
        g_printf ("object contributor name:%s\n", gupnp_didl_lite_contributor_get_name (contributor));

        g_object_unref (contributor);
    }
    g_list_free (artists);

    GList *authors = gupnp_didl_lite_object_get_authors (object);
    for (i = 0; i < g_list_length (authors); i++)
    {
        GUPnPDIDLLiteContributor *contributor = (GUPnPDIDLLiteContributor *) g_list_nth_data (authors, i);
        g_assert (contributor != NULL);
        g_printf ("object contributor role:%s\n", gupnp_didl_lite_contributor_get_role (contributor));
        g_printf ("object contributor name:%s\n", gupnp_didl_lite_contributor_get_name (contributor));

        g_object_unref (contributor);
    }
    g_list_free (authors);

    GList *descriptors = gupnp_didl_lite_object_get_descriptors (object);
    for (i = 0; i < g_list_length (descriptors); i++)
    {
        GUPnPDIDLLiteDescriptor *descriptor = (GUPnPDIDLLiteDescriptor *) g_list_nth_data (descriptors, i);
        g_assert (descriptor != NULL);
        g_printf ("object descriptor content:%s\n", gupnp_didl_lite_descriptor_get_content (descriptor));
        g_printf ("object descriptor id:%s\n", gupnp_didl_lite_descriptor_get_id (descriptor));
        g_printf ("object descriptor metadata type:%s\n", gupnp_didl_lite_descriptor_get_metadata_type (descriptor));
        g_printf ("object descriptor namespace:%s\n", gupnp_didl_lite_descriptor_get_name_space (descriptor));

        g_object_unref (descriptor);
    }
    g_list_free (descriptors);

    g_printf ("object write status:%s\n", gupnp_didl_lite_object_get_write_status (object));
    g_printf ("object genre:%s\n", gupnp_didl_lite_object_get_genre (object));
    g_printf ("object upnp class:%s\n", gupnp_didl_lite_object_get_upnp_class (object));
    g_printf ("object album:%s\n", gupnp_didl_lite_object_get_album (object));
    g_printf ("object album art:%s\n", gupnp_didl_lite_object_get_album_art (object));
    g_printf ("object description:%s\n", gupnp_didl_lite_object_get_description (object));
    g_printf ("object date:%s\n", gupnp_didl_lite_object_get_date (object));
    g_printf ("object track number:%d\n", gupnp_didl_lite_object_get_track_number (object));
    g_printf ("object dlna managerd:%d\n", gupnp_didl_lite_object_get_dlna_managed (object));
    
    g_printf ("object resources pass:\n");

}

void
didl_container_available_cb (GUPnPDIDLLiteParser *parser,
                             GUPnPDIDLLiteContainer *container,
                             gpointer user_data)
{
    //g_printf ("didl container available cb\n");

    GUPnPDIDLLiteObject *object = GUPNP_DIDL_LITE_OBJECT (container);

    GUPnPServiceProxy *proxy = (GUPnPServiceProxy *) user_data;
    g_assert (proxy != NULL);

    const char *id = gupnp_didl_lite_object_get_id (object);
    cds_browse_content (proxy, id);

//    test_didl_container (container);
}

void
didl_item_available_cb (GUPnPDIDLLiteParser *parser,
                        GUPnPDIDLLiteItem *item,
                        gpointer user_data)
{
    //g_printf ("didl item available cb\n");
    GUPnPDIDLLiteObject *object = GUPNP_DIDL_LITE_OBJECT (item);

    xmlNode *xml_node = gupnp_didl_lite_object_get_xml_node (object);
    g_assert (xml_node != NULL);

    while (xml_node != NULL)
    {
        if (g_strcmp0 (xml_node->name, "item") == 0)
        {
            xmlNode *item_children = xml_node->children;
            while (item_children != NULL)
            {
                if (g_strcmp0(item_children->name, "title") == 0)
                {
                    g_printf ("item title:%s\n", item_children->children->content);
                }
                else if (g_strcmp0(item_children->name, "class") == 0)
                {
                    g_printf ("item class:%s\n", item_children->children->content);
                }
                else if (g_strcmp0(item_children->name, "res") == 0)
                {
                    g_printf ("item res:%s\n", item_children->children->content);
                }
                else
                {
                    ;
                }

                item_children = item_children->next;
            }
        }
        xml_node = xml_node->next;
    }
    //test_didl_item (item);
}

void
didl_object_available_cb (GUPnPDIDLLiteParser *parser,
                          GUPnPDIDLLiteObject *object,
                          gpointer user_data)
{

    gboolean is_container = GUPNP_IS_DIDL_LITE_CONTAINER (object);
    gboolean is_item = GUPNP_IS_DIDL_LITE_ITEM (object);

    const char *id = gupnp_didl_lite_object_get_id (object);
    const char *parent = gupnp_didl_lite_object_get_parent_id (object);
    const char *title = gupnp_didl_lite_object_get_title (object);

    if (is_container)
    {
        g_printf ("\ndidl object is container\n");
        g_printf ("\tobject id:%s\tparent:%s\ttitle:%s\n", id, parent, title);

    } else if (is_item) {
        const char *creator = gupnp_didl_lite_object_get_creator (object);
        const char *artist = gupnp_didl_lite_object_get_artist (object);
        const char *author = gupnp_didl_lite_object_get_author (object);
        g_printf ("\ndidl object is item\n");
        g_printf ("\tobject id:%s\tparent:%s\ttitle:%s\n", id, parent, title);
        g_printf ("\tobject creator:%s\tartist:%s\tauthor:%s\n", creator, artist, author);

    } else {
        g_printf ("\ndidl object is not container or item\n");
    }

  //  test_didl_object (object);
}

void 
cds_browse_content (GUPnPServiceProxy *proxy, const char *id)
{
    char *result;
    int number_returned, total_matches,update_id;

    gupnp_service_proxy_send_action (proxy,
                                     "Browse",
                                     NULL,
                                     "ObjectID", G_TYPE_STRING, id,
                                     "BrowseFlag", G_TYPE_STRING, "BrowseDirectChildren", 
                                     "Filter", G_TYPE_STRING, "*",
                                     "StartingIndex", G_TYPE_UINT, 0,
                                     "RequestedCount", G_TYPE_UINT, 0,
                                     "SortCriteria", G_TYPE_STRING, "",
                                     NULL,
                                     "Result", G_TYPE_STRING, &result,
                                     "NumberReturned", G_TYPE_UINT, &number_returned,
                                     "TotalMatches", G_TYPE_UINT, &total_matches,
                                     "UpdateID", G_TYPE_UINT, &update_id,
                                     NULL);

    g_printf ("\n%s browse number returned:%d\n", id, number_returned);
    //g_printf ("browse total_matches:%d\n", total_matches);
    //g_printf ("browse update id:%d\n", update_id);
    g_printf ("\tbrowse result:%s\n", result);
    
    GUPnPDIDLLiteParser *parser = gupnp_didl_lite_parser_new ();
    g_assert (parser != NULL);

    g_signal_connect (parser,
                      "container-available",
                      G_CALLBACK (didl_container_available_cb),
                      proxy);

    g_signal_connect (parser,
                      "item-available",
                      G_CALLBACK (didl_item_available_cb),
                      proxy);

    g_signal_connect (parser,
                      "object-available",
                      G_CALLBACK (didl_object_available_cb),
                      proxy);

    gupnp_didl_lite_parser_parse_didl (parser, result, NULL);

    g_free (result);
}

void 
cds_browse_root (GUPnPServiceProxy *proxy)
{
    cds_browse_content (proxy, "0");
}

void 
on_device_proxy_available (GUPnPControlPoint *control_point,
                           GUPnPDeviceProxy *proxy,
                           gpointer user_data)
{
    const char* udn = (const char *) user_data;
    g_assert (udn != NULL);

    g_printf ("on device proxy available for:%s\n", udn);

    GUPnPDeviceInfo *d_info = GUPNP_DEVICE_INFO (proxy);

    g_printf ("device udn:%s\n", gupnp_device_info_get_udn (d_info));
    g_printf ("device location:%s\n", gupnp_device_info_get_location (d_info));
}

void 
on_device_proxy_unavailable (GUPnPControlPoint *control_point,
                           GUPnPDeviceProxy *proxy,
                           gpointer user_data)
{
    const char* udn = (const char *) user_data;
    g_assert (udn != NULL);

    g_printf ("on device proxy unavailable for:%s\n", udn);
}

void 
on_service_proxy_available (GUPnPControlPoint *control_point,
                           GUPnPServiceProxy *proxy,
                           gpointer user_data)
{
    const char* udn = (const char *) user_data;
    g_assert (udn != NULL);

    g_printf ("on service proxy available for:%s\n", udn);
}

void 
on_service_proxy_unavailable (GUPnPControlPoint *control_point,
                           GUPnPServiceProxy *proxy,
                           gpointer user_data)
{
    const char* udn = (const char *) user_data;
    g_assert (udn != NULL);

    g_printf ("on service proxy unavailable:%s\n", udn);
}

void 
device_proxy_available_cb (GUPnPControlPoint *control_point,
                           GUPnPDeviceProxy *proxy,
                           gpointer user_data)
{
    GUPnPDeviceInfo *device_info = GUPNP_DEVICE_INFO (proxy);

    const char *device_type = gupnp_device_info_get_device_type (device_info);

    if (g_str_has_prefix (device_type, "urn:schemas-upnp-org:device:MediaServer:3"))
    {
        g_printf ("%s\n", "device proxy available cb");

        g_printf ("device type:%s\n", gupnp_device_info_get_device_type (device_info));

        const char *udn = gupnp_device_info_get_udn (device_info);
        g_assert (udn != NULL);

        GUPnPContext *ctx = gupnp_control_point_get_context (control_point);
        g_assert (ctx != NULL);

        GUPnPControlPoint *d_cp = gupnp_control_point_new (ctx, device_type);
        g_assert (d_cp != NULL);

        g_signal_connect (d_cp,
                         "device-proxy-available",
                         G_CALLBACK (on_device_proxy_available),
                         (gpointer) udn);

        g_signal_connect (d_cp, 
                         "device-proxy-unavailable",
                         G_CALLBACK (on_device_proxy_unavailable),
                         (gpointer) udn);

        g_signal_connect (d_cp, 
                         "service-proxy-available",
                         G_CALLBACK (on_service_proxy_available),
                         (gpointer) udn);

        g_signal_connect (d_cp,
                         "service-proxy-unavailable",
                         G_CALLBACK (on_service_proxy_unavailable),
                         (gpointer) udn);

        gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (d_cp), TRUE);

        gupnp_context_manager_manage_control_point (context_manager, d_cp);

        /*
        g_printf ("\nlist device for %s:\n", udn);
        GList *device_list = gupnp_device_info_list_devices (device_info);
        if (device_list != NULL)
        {
            int j;
            for (j = 0; j < g_list_length (device_list); j++)
            {
                GUPnPDeviceInfo *dev_info = (GUPnPDeviceInfo *) g_list_nth_data (device_list, j);
                g_assert (dev_info != NULL);
                g_printf ("direct child device location:%s\n", gupnp_device_info_get_location (dev_info));
                g_object_unref (dev_info);
            }
        }
        g_list_free (device_list);
        g_printf ("list device for %s finish\n", udn);
        */

        //g_printf ("\nlist service for %s:\n", udn);
        GList *service_list = gupnp_device_info_list_services (device_info);
        if (service_list != NULL)
        {
            int m;
            for (m = 0; m < g_list_length (service_list); ++m)
            {
                GUPnPServiceProxy *service_proxy = g_list_nth_data (service_list, m);
                GUPnPServiceInfo *service_info =  GUPNP_SERVICE_INFO (service_proxy);

                char *service_id = gupnp_service_info_get_id (service_info);

                if (g_str_has_prefix (service_id, "urn:upnp-org:serviceId:ContentDirectory"))
                {
                    g_printf ("content directory service\n\n");
                    cds_browse_root (service_proxy);
                }
                g_free (service_id);
                g_object_unref (service_proxy);
            }
        }
        g_list_free (service_list);
        //g_printf ("list service for %s finish\n\n", udn);

        g_object_unref (d_cp);
    }
}

void
device_proxy_unavailable_cb (GUPnPControlPoint *control_point,
                             GUPnPDeviceProxy *proxy,
                             gpointer user_data)
{
    g_printf ("%s\n", "device proxy unavailable cb");
}

void 
service_proxy_available_cb (GUPnPControlPoint *control_point,
                            GUPnPServiceProxy *proxy,
                            gpointer user_data)
{
    g_printf ("%s\n", "service proxy available cb");
}


void 
service_proxy_unavailable_cb (GUPnPControlPoint *control_point, 
                              GUPnPServiceProxy *proxy,
                              gpointer user_data)
{
    g_printf ("%s\n", "service proxy unavailable cb");
}

static void
on_context_available(GUPnPContextManager *context_manager,
                     GUPnPContext *context,
                     gpointer user_data)
{
    g_printf ("%s\n", "context available cb");

    GUPnPControlPoint *root_cp = gupnp_control_point_new (context, "upnp:rootdevice");
    g_assert (root_cp != NULL);

    GSSDPClient *client = gssdp_resource_browser_get_client (GSSDP_RESOURCE_BROWSER (root_cp));

    if(g_strcmp0(gssdp_client_get_interface (client), "lo") != 0)
    {

        g_signal_connect (root_cp,
                          "device-proxy-available",
                          G_CALLBACK (device_proxy_available_cb),
                          NULL);

        g_signal_connect (root_cp, 
                          "device-proxy-unavailable",
                          G_CALLBACK (device_proxy_unavailable_cb),
                          NULL);

        g_signal_connect (root_cp, 
                          "service-proxy-available",
                          G_CALLBACK (service_proxy_available_cb),
                          NULL);

        g_signal_connect (root_cp,
                          "service-proxy-unavailable",
                          G_CALLBACK (service_proxy_unavailable_cb),
                          NULL);

        gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (root_cp), TRUE);
        gupnp_context_manager_manage_control_point (context_manager, root_cp);
    }

    g_object_unref (root_cp);
}

static void
on_context_unavailable(GUPnPContextManager *context_manager,
                       GUPnPContext *context,
                       gpointer user_data)
{
    g_printf ("%s\n", "context unavailable cb");
}

int 
main (int argc, char **argv)
{
    GMainLoop *mainloop = g_main_loop_new (NULL, FALSE);

    context_manager = gupnp_context_manager_new (NULL, 0);
    g_assert (context_manager != NULL);

    g_signal_connect (context_manager,
                      "context-available",
                      G_CALLBACK (on_context_available),
                      NULL);

    g_signal_connect (context_manager,
                      "context-unavailable",
                      G_CALLBACK (on_context_unavailable),
                      NULL);

    g_main_loop_run (mainloop);

    return 0;
}
