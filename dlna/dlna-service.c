#include "dlna-service.h"
#include <string.h>

void
print_service_info (GUPnPServiceInfo *service_info)
{
    g_assert (service_info != NULL);

    g_printf ("\nprint service info begin\n");
    g_printf ("service info location:%s\n", gupnp_service_info_get_location (service_info));
    g_printf ("service info udn:%s\n", gupnp_service_info_get_udn (service_info));
    g_printf ("service info service type:%s\n", gupnp_service_info_get_service_type (service_info));

    char *id = gupnp_service_info_get_id (service_info);
    g_printf ("service info id:%s\n", id);
    g_free (id);

    char *scpd_url = gupnp_service_info_get_scpd_url (service_info);
    g_printf ("service info scpd url:%s\n", scpd_url);

    char *control_url = gupnp_service_info_get_control_url (service_info);
    g_printf ("service info control url:%s\n", control_url);
    g_free (control_url);

    char *event_url = gupnp_service_info_get_event_subscription_url (service_info);
    g_printf ("service info event url:%s\n", event_url);
    g_free (event_url);

    GUPnPServiceIntrospection *introspect = gupnp_service_info_get_introspection (service_info, NULL);
    g_assert (introspect != NULL);

    GList *action_names = gupnp_service_introspection_list_action_names (introspect);
    if (action_names != NULL)
    {
        int i;
        for (i = 0; i < g_list_length (action_names); i++)
        {
            const char *action_name = (const char *)g_list_nth_data (action_names, i);
            g_printf ("%d th action name:%s\n", i, action_name);

            const GUPnPServiceActionInfo *action_info = gupnp_service_introspection_get_action (introspect, action_name);
            g_assert (action_info != NULL);

            GList *arguments = action_info->arguments;
            if (arguments != NULL)
            {
                int j;
                for (j = 0; j < g_list_length (arguments); j++)
                {
                    GUPnPServiceActionArgInfo *arg_info = (GUPnPServiceActionArgInfo *) g_list_nth_data (arguments, j);
                    g_printf ("arg name:%s\n", arg_info->name);
                    g_printf ("arg direction:%d\n", arg_info->direction);
                    g_printf ("arg related state variable:%s\n", arg_info->related_state_variable);
                    g_printf ("arg retval:%d\n", arg_info->retval);
                }
            }
        }
    }
    
    /***
    const GList *state_variable_names = gupnp_service_introspection_list_state_variable_names (introspect);
    if (state_variable_names != NULL)
    {
        int j;
        for (j = 0; j < g_list_length (state_variable_names); j++)
        {
            g_printf ("%d th state variable name:%s\n", j, (char *)g_list_nth_data (state_variable_names, j));
        }
    }
    ***/

    GList *state_variables = gupnp_service_introspection_list_state_variables (introspect);
    if (state_variables != NULL)
    {
        int k;
        for (k = 0; k < g_list_length (state_variables); k++)
        {
           GUPnPServiceStateVariableInfo *var_info = (GUPnPServiceStateVariableInfo *) g_list_nth_data (state_variables, k);
           g_assert (var_info != NULL);
           g_printf ("state var name:%s\n", var_info->name);
           g_printf ("state var send events:%d\n", var_info->send_events);
           g_printf ("state var is num:%d\n", var_info->is_numeric);
        }
    }

    g_printf ("print service info end\n");
}

void test_state_variable (GUPnPServiceInfo *service_info)
{
    g_printf ("test state variable\n");
}

void test_content_directory_browse (GUPnPServiceProxy *proxy)
{
    char *result;
    int number_returned, total_matches,update_id;

    gupnp_service_proxy_send_action (proxy,
                                     "Browse",
                                     NULL,
                                     "ObjectID", G_TYPE_STRING, "0",
                                     "BrowseFlag", G_TYPE_STRING, "BrowseMetadata", 
                                     "Filter", G_TYPE_STRING, "*",
                                     "StartingIndex", G_TYPE_UINT, 0,
                                     "RequestedCount", G_TYPE_UINT, 0,
                                     "SortCriteria", G_TYPE_STRING, "",
                                     NULL,
                                     "Result", G_TYPE_STRING, &result,
                                     "NumberReturned", G_TYPE_UINT, &number_returned,
                                     "TotalMatches", G_TYPE_UINT, &total_matches,
                                     "UpdateID", G_TYPE_UINT, &update_id,
                                     NULL);

    g_printf ("browse result:%s\n", result);
    g_printf ("browse number returned:%d\n", number_returned);
    g_printf ("browse total_matches:%d\n", total_matches);
    g_printf ("browse update id:%d\n", update_id);

    //g_free (result);
}

void test_content_directory_browse_children (GUPnPServiceProxy *proxy)
{
    char *result;
    int number_returned, total_matches,update_id;

    gupnp_service_proxy_send_action (proxy,
                                     "Browse",
                                     NULL,
                                     "ObjectID", G_TYPE_STRING, "0",
                                     "BrowseFlag", G_TYPE_STRING, "BrowseDirectChildren", 
                                     "Filter", G_TYPE_STRING, "*",
                                     "StartingIndex", G_TYPE_UINT, 0,
                                     "RequestedCount", G_TYPE_UINT, 0,
                                     "SortCriteria", G_TYPE_STRING, "",
                                     NULL,
                                     "Result", G_TYPE_STRING, &result,
                                     "NumberReturned", G_TYPE_UINT, &number_returned,
                                     "TotalMatches", G_TYPE_UINT, &total_matches,
                                     "UpdateID", G_TYPE_UINT, &update_id,
                                     NULL);

    g_printf ("browse result:%s\n", result);
    g_printf ("browse number returned:%d\n", number_returned);
    g_printf ("browse total_matches:%d\n", total_matches);
    g_printf ("browse update id:%d\n", update_id);

    //g_free (result);
}

void test_cds_sort_capabilities (GUPnPServiceProxy *proxy)
{
    char *sort_capabilities;

    gupnp_service_proxy_send_action (proxy,
                                     "GetSortCapabilities",
                                     NULL,
                                     NULL,
                                     "SortCaps", G_TYPE_STRING, &sort_capabilities,
                                     NULL);

    g_printf ("get sort capabilities:%s\n", sort_capabilities);
}

void test_cds_search_capabilities (GUPnPServiceProxy *proxy)
{

    char **search_capabilities;

    gupnp_service_proxy_send_action (proxy,
                                     "GetSearchCapabilities",
                                     NULL,
                                     NULL,
                                     "SearchCaps", G_TYPE_STRING, search_capabilities,
                                     NULL);

    g_printf ("get search capabilities:%s\n", search_capabilities);
}

void test_cds_system_update_id (GUPnPServiceProxy *proxy)
{
    guint system_update_id;

    gupnp_service_proxy_send_action (proxy, 
                                    "GetSystemUpdateID",
                                    NULL,
                                    NULL,
                                    "Id", G_TYPE_UINT, &system_update_id,
                                    NULL);

    g_printf ("get system update id:%d\n", system_update_id);
}

void test_cds_feature_list (GUPnPServiceProxy *proxy)
{

    char *feature_list;

    gupnp_service_proxy_send_action (proxy,
                                     "GetFeatureList",
                                     NULL,
                                     NULL,
                                     "FeatureList", G_TYPE_STRING, feature_list,
                                     NULL);

    g_printf ("get feature list:%s\n", feature_list);
}

void test_cds_subscribe (GUPnPServiceProxy *proxy)
{
    gboolean suscribed;

    suscribed = gupnp_service_proxy_get_subscribed (proxy);
    g_printf ("cds subscribed:%d\n", suscribed);

    gupnp_service_proxy_set_subscribed (proxy, TRUE);

    suscribed = gupnp_service_proxy_get_subscribed (proxy);
    g_printf ("cds subscribed:%d\n", suscribed);
}


void on_cds_state_variable_change (GUPnPServiceProxy *proxy,
                                   const char *variable_name, 
                                   GValue *value,
                                   gpointer user_data)
{
    g_printf ("on cds state variable change\n");

    g_printf ("variable name:%s\n", variable_name);

    GValue str_value;

    memset (&str_value, 0, sizeof(GValue));
    g_value_init (&str_value, G_TYPE_STRING);

    g_value_transform (value, &str_value);

    g_printf ("variable new value:%s\n", g_value_get_string(&str_value));

    g_value_unset (&str_value);
}


void test_cds_state_variable (GUPnPServiceProxy *proxy)
{
    GUPnPServiceInfo *service_info = (GUPnPServiceInfo *)proxy;
    g_assert (service_info != NULL);

    GUPnPServiceIntrospection *introspect = gupnp_service_info_get_introspection (service_info, NULL);
    g_assert (introspect != NULL);


    GList *state_variables = gupnp_service_introspection_list_state_variables (introspect);
    if (state_variables != NULL)
    {
        int k;
        for (k = 0; k < g_list_length (state_variables); k++)
        {
           GUPnPServiceStateVariableInfo *var_info = (GUPnPServiceStateVariableInfo *) g_list_nth_data (state_variables, k);
           g_assert (var_info != NULL);

           gupnp_service_proxy_add_notify (proxy,
                                           var_info->name,
                                           var_info->type,
                                           on_cds_state_variable_change,
                                           NULL);
           //g_printf ("state var name:%s\n", var_info->name);
           //g_printf ("state var send events:%d\n", var_info->send_events);
           //g_printf ("state var is num:%d\n", var_info->is_numeric);
        }
    }

}

void test_content_directory (GUPnPServiceProxy *proxy)
{
    g_printf ("test content directory\n");

    test_content_directory_browse_children (proxy);
    test_cds_subscribe (proxy);

    test_cds_state_variable (proxy);
    //test_cds_sort_capabilities (proxy);
    //test_cds_search_capabilities (proxy);
    //test_cds_system_update_id (proxy);
}
