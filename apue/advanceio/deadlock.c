#include <fcntl.h>

static void
lockabyte (const char *name, int fd, off_t offset)
{
    if (writew_lock (fd, offset, SEEK_SET, 1) < 0) {
        printf ("write lock error %s\n", name);
    }

    printf ("%s:got the lock, byte %ld\n", name, offset);
}

int main (void)
{
    int fd;
    pid_t pid;

    if ((fd = create ("templock", FILE_MODE)) < 0) {
        printf ("create error\n");
    }

    if (write (fd, "ab", 2) != 2) {
        printf ("write error\n");
    }

    TELL_WAIT ();
    
    if ((pid = fork()) < 0) {
        printf ("fork error\n");
    } else if (pid == 0) {
        lockabyte ("child", fd, 0);
        TELL_PARENT (getppid());
        WAIT_PARENT ();
        lockabyte ("child", fd, 1);
    } else {
        lockabyte ("parent", fd, 1);
        TELL_CHILD (pid);
        WAIT_CHILD ();
        lockabyte ("parent", fd, 0);
    }

    exit (0);
}
