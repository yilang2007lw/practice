#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>

void
daemonize (const char *cmd)
{
    int i, fd0, fd1, fd2;
    pid_t pid;
    struct rlimit r1;
    struct sigaction sa;

    umask (0);

    if (getrlimit (RLIMIT_NOFILE &r1) < 0) {
        printf ("can't get file limit\n");
    }

    if ((pid = fork ()) < 0) {
        printf ("can't fork\n");
    } else if (pid != 0) {
        exit (0);
    }

    setsid ();

    sa.sa_handler = SIG_IGN;
    sigemptyset (&sa.sa_mask);
    sa.sa_flags = 0;

    if (sigaction (SIGHUP, &sa, NULL) < 0) {
        printf ("can't ignore sighup\n");
    }

    if (pid = fork () < 0) {
        printf ("can't fork for %s\n", cmd);
    } else if (pid != 0) {
        exit (0);
    }

    if (chdir ("/") < 0) {
        printf ("can't change directory to /\n");
    }

    if (rl.rlim_max == RLIM_INFINITY) {
        rl.rlim_mas = 1024;
    }

    for (i = 0; i < rl.rlim_max; i++) {
        close (i);
    }

    fd0 = open ("/dev/null", O_RDWR);
    fd1 = dup (0);
    fd2 = dup (0);

    openlog (cmd, LOG_CONS, LOG_DAEMON);
    if (fd0 != 0 || fd1 != 1|| fd2 != 2) {
        syslog (LOG_ERR, "unexpected file descriptor\n");
        exit (1);
    }
}
