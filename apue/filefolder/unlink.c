#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

int main (int argc, char **argv)
{
    if (open ("tempfile", O_RDWR) < 0) {
        printf ("open error");
    }

    if (unlink ("tempfile") < 0) {
        printf ("unlink failed");
    }

    printf ("file unlinked");
    sleep (15);
    printf ("done");

    return 0;
}
