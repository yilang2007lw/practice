#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

char *
path_alloc (int *size)
{
    char *p;

    p = malloc (256);
    if (p == NULL) {
        printf ("malloc failed\n");
        *size = 0;
    } else {
        *size = 256;
    }

    return p;
}

int main (int argc, char **argv)
{
    char *path = NULL;
    int len;

    path = path_alloc (&len);

    if (chdir ("/tmp") < 0) {
        printf ("chdir failed\n");
    }
    
    if (getcwd (path, len) == NULL) {
        printf ("getcwd failed\n");
    }

    printf ("cwd:%s\n", path);

    return 0;
}
