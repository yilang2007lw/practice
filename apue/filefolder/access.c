#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

int main (int argc, char **argv)
{
    if (argc != 2) {
        printf ("invalid argv");
        return -1;
    }

    if (access (argv[1], X_OK) < 0) {
        printf ("access failed");
    } else {
        printf ("access succeed");
    }

    if (open (argv[1], O_RDONLY) < 0) {
        printf ("open failed");
    } else {
        printf ("open succeed");
    }

    return 0;
}
