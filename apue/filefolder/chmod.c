#include <stdio.h>
#include <sys/stat.h>

int main (int argc, char **argv)
{
    struct stat buf;

    if (stat ("foo", &buf) < 0) {
        printf ("stat failed");
    }

    if (chmod ("foo", (buf.st_mode & ~S_IXGRP) | S_ISGID) < 0) {
        printf ("chmod foo failed");
    }

    if (chmod ("bar", S_IRUSR |S_IWUSR |S_IRGRP |S_IROTH) < 0) {
        printf ("chmod bar failed");
    }

    return 0;
}
