#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <pwd.h>
#include <sys/types.h>

static void
my_alarm (int signo)
{
    struct passwd *rootptr;
    printf ("in signal handler\n");
    
    if ((rootptr = getpwnam ("root")) == NULL) {
        printf ("getpwname for root failed\n");
    }
    printf ("in my_alarm:%s\n", rootptr->pw_name);

    alarm (1);
}

int main (void)
{
    struct passwd *ptr;

    signal (SIGALRM, my_alarm);
    alarm (1);

    for (; ;) {
        if ((ptr = getpwnam ("longwei")) == NULL) {
            printf ("getpwnam for longwei failed\n");
        }

        if (strcmp (ptr->pw_name, "longwei") != 0) {
            printf ("return value corrupted, pw_name = %s\n", ptr->pw_name);
        }

        //printf ("in main:%s\n", ptr->pw_name);
    }
}
