#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

static void sig_quit (int);

int main (void)
{
    sigset_t newmask, oldmask, pendmask;

    if (signal (SIGQUIT, sig_quit) == SIG_ERR) {
        printf ("can't catch sig quit\n");
    }

    sigemptyset (&newmask);
    sigaddset (&newmask, SIGQUIT);

    if (sigprocmask (SIG_BLOCK, &newmask, &oldmask) < 0) {
        printf ("sig block error\n");
    }

    sleep (5);

    if (sigpending (&pendmask) < 0) {
        printf ("sig pending error\n");
    }

    if (sigismember (&pendmask, SIGQUIT)) {
        printf ("sig quit pending\nq");
    }

    if (sigprocmask (SIG_SETMASK, &oldmask, NULL) < 0) {
        printf ("sig set mask error\n");
    }

    printf ("sig quit unblocked\n");

    sleep (5);

    exit (0);
}

static void 
sig_quit (int signo)
{
    printf ("caught sigquit\n");
    if (signal (SIGQUIT, SIG_DFL) == SIG_ERR) {
        printf ("can't reset sigquit\n");
    }
}
