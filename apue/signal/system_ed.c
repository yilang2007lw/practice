#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

static void
sig_int (int signo)
{
    printf ("caught sigint");
}

static void
sig_chld (int signo)
{
    printf ("caught sig child");
}

int main (void)
{
    if (signal (SIGINT, sig_int) == SIG_ERR) {
        printf ("sig int error\n");
    }

    if (signal (SIGCHLD, sig_chld) == SIG_ERR) {
        printf ("sig chld error\n");
    }

    if (system ("/bin/ed") < 0) {
        printf ("system error\n");
    }

    exit (0);
}
