#include <sys/wait.h>
#include <stdio.h>

int main (void)
{
    int status;

    if ((status = msystem ("date")) < 0) {
        printf ("system error");
    }
    pr_exit (status);

    if ((status = msystem ("nosuchcommand")) < 0) {
        printf ("system error");
    }
    pr_exit (status);

    if ((status = msystem ("who; exit 44")) < 0) {
        printf ("system error");
    }
    pr_exit (status);

    return 0;
}
