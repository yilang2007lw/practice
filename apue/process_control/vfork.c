#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int glob = 6;
char buf[] = "a write to stdout\n";

int main (void)
{
    int var;
    pid_t pid;

    var = 88;
    if (write (STDOUT_FILENO, buf, sizeof(buf) - 1) != sizeof(buf) - 1) {
        printf ("write error\n");
    }
    printf ("before vfork\n");

    if ((pid = vfork()) < 0) {
        printf ("vfork error\n");
    } else if (pid == 0) {
        glob++;
        var++;
//        _exit (0);
        exit (0);
    }

    printf ("get data\n");
    glob++;
    printf ("pid=%d, glob=%d, var=%d\n", getpid(), glob, var);

    return 0;
}
