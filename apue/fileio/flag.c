#include <fcntl.h>
#include <stdio.h>

int main (int argc, char **argv)
{
    int val;

    if (argc != 2) {
        printf ("invalid arguments\n");
    }

    if ((val = fcntl (atoi (argv[1]), F_GETFL, 0)) < 0) {
        printf ("get file flag failed\n");
    }

    switch (val & O_ACCMODE) {
    case O_RDONLY:
        printf ("readonly");
        break;
    case O_WRONLY:
        printf ("write only");
        break;  
    case O_RDWR:
        printf ("read write");
        break;
    default:
        printf ("unknown");
        break;
    }
    
    return 0;
}
