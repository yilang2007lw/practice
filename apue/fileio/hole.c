#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>

int main (int argc, char **argv)
{
    char buf1[] = "abcdefghij";
    char buf2[] = "ABCDEFGHIJ";
    int fd;

    if ((fd = creat ("file.hole", 0777)) < 0) {
        printf ("create file failed\n");
    }

    if (write (fd, buf1, 10) != 10) {
        printf ("write buf1 failed\n");
    }

    if (lseek (fd, 1024, SEEK_CUR) == -1) {
        printf ("lseek failed\n");
    }

    if (write (fd, buf2, 10) != 10) {
        printf ("write buf2 failed\n");
    }

    exit (0);
}
