#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>

int main (int argc, char **argv)
{
    if (lseek (STDIN_FILENO, 0, SEEK_CUR) == -1) {
        printf ("seek cur failed\n");
    } else {
        printf ("seek cur ok\n");
    }
    
    //if (fdopen (STDIN_FILENO, "r") == NULL) {
    //    printf ("fdopen failed\n");
    //}

    if (lseek (STDIN_FILENO, 0, SEEK_SET) == -1) {
        printf ("seek set failed\n");
    } else {
        printf ("seek set ok\n");
    }

    exit (0);
}
