#include <stdio.h>
#include <string.h>

#define BUFSIZE 30

int main (int argc, char **argv)
{
    char buf[BUFSIZE];

    memset (buf, 0, BUFSIZE);

    printf ("buf:%s\n", buf);
    printf ("\nbuf end\n");

    setbuf (stdout, buf);

    printf ("Hello World 1\n");
    printf ("Hello World 2\n");
    printf ("Hello World 3\n");
    printf ("Hello World 4\n");

    return 0;
}
