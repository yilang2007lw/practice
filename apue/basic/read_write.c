#include <unistd.h>
#include <stdio.h>

#define BUFSIZE 20 

int main (int argc, char **argv)
{
    int n;
    char buf[BUFSIZE];

    while ((n = read (STDIN_FILENO, buf, BUFSIZE)) > 0) {
        printf ("read:%s\n", buf);
        printf ("read %d\n", n);
        printf ("write\n");
        if (write (STDOUT_FILENO, buf, n) != n) {
            printf ("write error\n");
        }
    }

    if (n < 0) {
        printf ("read error\n");
    }

    //return 0;
    _exit (0);
}
