#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

#define BUFSIZE 1024

int i;

int main (int argc, char *argv)
{
    char buf[BUFSIZE];
    pid_t pid;
    int status;

    printf ("i:%d\n", i);
    printf ("status:%d\n", status);

    while (fgets (buf, BUFSIZE, stdin) != NULL) {
        if (buf[strlen(buf) - 1] == '\n')
            buf[strlen(buf) - 1] = 0;

        if ((pid = fork ()) < 0) {
            printf ("fork failed\n");

        } else if (pid == 0) {
            execlp (buf, buf, (char *) 0);
            printf ("execute:%s\n", buf);
            exit (127);
        }

        if ((pid = waitpid (pid, &status, 0)) < 0) {
            printf ("waitpid error\n");
        }
        printf ("%%");
    }

    exit (0);
}
