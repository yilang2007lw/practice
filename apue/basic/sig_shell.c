#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFSIZE 1024

static void sig_int (int);

int main (int argc, char **argv)
{
    char buf[BUFSIZE];
    pid_t pid;
    int status;

    memset (buf, 0, BUFSIZE);

    if (signal (SIGINT, sig_int) == SIG_ERR) {
        printf ("signal failed\n");
    }

    printf ("%%");

    while (fgets (buf, BUFSIZE, stdin) != NULL) {
        if (buf[strlen(buf) - 1] == '\n') {
            buf[strlen(buf) -1] = 0;
        }

        if ((pid = fork()) < 0) {
            fprintf (stderr, "fork failed");

        } else if (pid == 0) {
            execlp (buf, buf, (char *) 0);
            printf ("couldn't execute:%s\n", buf);
            exit (127);
        }

        if ((pid = waitpid (pid, &status, 0)) < 0) {
            fprintf (stderr, "wait child error");
        }

        printf ("%%");
    }

    return 0;
}

void sig_int (int signum) 
{
    printf ("sig int\n");
}
