#include <stdio.h>

int main (int argc, char **argv)
{
    char buf[4096];

    while (fgets (buf, 4096, stdin) != NULL) {
        if (fputs (buf, stdout) == EOF) {
            printf ("output error");
        }
    }

    if (ferror (stdin)) {
        printf ("input error");
    }

    return 0;
}
