call pathogen#infect()
syntax on
filetype plugin on
filetype indent on

set t_Co=256
set nocompatible
set shiftwidth=4
set tabstop=4
set softtabstop=4
set smarttab
set expandtab
set lbr
set tw=500
set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

set nu
set autochdir
set autoread
set hlsearch
set incsearch
set ignorecase
set smartcase

set nobackup
set noswapfile
set nowb

set encoding=utf8
set ffs=unix,dos,mac "Default file types

let mapleader = ","
let g:mapleader = ","
nmap <leader>w :w!<CR>

map <C-F12> :!etags -R .<CR>

nmap Hw :split<CR>
nmap Vw :vsplit<CR>
nmap K :close<CR>
nmap Ho :split 
nmap Vo :vsplit 

nmap <C-p> :tabprevious<CR>
nmap <C-n> :tabnext<CR>
nmap <C-l> :tablast<CR>
nmap <C-O> :tabnew 
nmap <C-k> :q!<CR>

set showtabline=2  " 0, 1 or 2; when to use a tab pages line
set tabline=%!MyTabLine()  " custom tab pages line

function! MyTabLine()
  let s = ''
  let t = tabpagenr()
  let i = 1
  while i <= tabpagenr('$')
    let buflist = tabpagebuflist(i)
    let winnr = tabpagewinnr(i)
    let s .= '%' . i . 'T'
    let s .= (i == t ? '%#TabLineSel#' : '%#TabLine#')
    let bufnr = buflist[winnr - 1]
    let file = bufname(bufnr)
    let buftype = getbufvar(bufnr, 'buftype')
    if buftype == 'nofile'
      if file =~ '\/.'
        let file = substitute(file, '.*\/\ze.', '', '')
      endif
    else
      let file = fnamemodify(file, ':p:t')
    endif
    if file == ''
      let file = '[No Name]'
    endif
    let s .= string(i) . ":"
    let file = strpart(file, 0, 10)
    let s .= file
    let i = i + 1
  endwhile
  let s .= '%T%#TabLineFill#%='
  let s .= (tabpagenr('$') > 1 ? '%999XX' : 'X')
  return s
endfunction

map <leader>1 1gt
map <leader>2 2gt
map <leader>3 3gt
map <leader>4 4gt
map <leader>5 5gt
map <leader>6 6gt
map <leader>7 7gt
map <leader>8 8gt
map <leader>9 9gt
map <leader>0 10gt

au BufWritePost *.coffee :!make >/dev/null
map <F6> <Esc>:setlocal spell spelllang=en_us<CR>
map <F7> <Esc>:setlocal nospell<CR>

