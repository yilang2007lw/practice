#include "utils.h"

static const struct table_entry {
	const char *extension;
	const char *content_type;
} content_type_table[] = {
	{ "txt", "text/plain" },
	{ "c", "text/plain" },
	{ "h", "text/plain" },
	{ "html", "text/html" },
	{ "htm", "text/htm" },
	{ "css", "text/css" },
	{ "gif", "image/gif" },
	{ "jpg", "image/jpeg" },
	{ "jpeg", "image/jpeg" },
	{ "png", "image/png" },
	{ "pdf", "application/pdf" },
	{ "ps", "application/postsript" },
	{ NULL, NULL },
};

char* evhttp_cmd_type_to_string(enum evhttp_cmd_type cmd_type)
{
    char *cmdtype;
    
    switch(cmd_type){

    case EVHTTP_REQ_GET:
        cmdtype = "get";
        break;
    case EVHTTP_REQ_POST:
        cmdtype = "post";
        break;
    case EVHTTP_REQ_HEAD:
        cmdtype = "head";
        break;
    case EVHTTP_REQ_PUT:
        cmdtype = "put";
        break;
    case EVHTTP_REQ_DELETE:
        cmdtype = "delete";
        break;
    case EVHTTP_REQ_OPTIONS:
        cmdtype = "options";
        break;
    case EVHTTP_REQ_TRACE:
        cmdtype = "trace";
        break;
    case EVHTTP_REQ_CONNECT:
        cmdtype = "connect";
        break;
    case EVHTTP_REQ_PATCH:
        cmdtype = "patch";
        break;
    default:
        cmdtype = "unknown";
        break;
    }

    return cmdtype;
}

char* guess_mime_from_ext(char *path)
{
    char *mime;
    const struct table_entry *ent;

    char *ext = strrchr(path, '.');

    for(ent = content_type_table[0]; ent->extension; ++ent){
        if((strcasecmp(ext, ent->extension)) == 0){
            mime = ent->content_type;
            return mime;
        }
    }

    return "application/misc";
}
