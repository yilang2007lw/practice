#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>

#include <event2/event.h>
#include <event2/http.h>
#include <event2/buffer.h>
#include <event2/util.h>
#include <event2/keyvalq_struct.h>

char * WEB_ROOT = "." ;

void
test_request_cb(struct evhttp_request *req, void *arg)
{
    fprintf(stdout, "received request:%s\n", (char *)arg);

    struct evbuffer *evb = evbuffer_new();

    evbuffer_add_printf(evb, 
                        "<html><head><title>This is a Test</title></head>"
                        "<body><h1>This is a Test </h1></body></html>");

    evhttp_send_reply(req, 200, "OK", evb);
}

void
html_request_cb(struct evhttp_request *req, void *arg)
{

    struct evbuffer *evb = evbuffer_new();
    
    int fd;
    struct stat st;

    if((fd = open("/home/yilang/test.html", O_RDWR)) < 0){
        perror("open /home/yilang/test.html failed");
        return ;
    }
    
    if((fstat(fd, &st) < 0)){
        perror("fstat");
        return ;
    }
    
    /* printf("response html file size:%d\n", (int)st.st_size); */

    if((evbuffer_add_file(evb, fd, 0, st.st_size)) == -1){
        perror("evbuffer_add_file");
        return ;
    }

    evhttp_send_reply(req, 200, "ok", evb);
}

char*
parse_evhttp_cmd_type(enum evhttp_cmd_type cmd)
{
    char *cmdtype;

    switch(cmd){

    case EVHTTP_REQ_GET:
        cmdtype = "GET";
        break;
    case EVHTTP_REQ_POST:
        cmdtype = "POST";
        break;
    case EVHTTP_REQ_HEAD:
        cmdtype = "HEAD";
        break;
    case EVHTTP_REQ_PUT:
        cmdtype = "PUT";
        break;
    case EVHTTP_REQ_DELETE:
        cmdtype = "DELETE";
        break;
    case EVHTTP_REQ_OPTIONS:
        cmdtype = "OPTIONS";
        break;
    case EVHTTP_REQ_TRACE:
        cmdtype = "TRACE";
        break;
    case EVHTTP_REQ_CONNECT:
        cmdtype = "CONNECT";
        break;
    case EVHTTP_REQ_PATCH:
        cmdtype = "PATCH";
        break;
    default:
        cmdtype = "UNKNOWN";
        break;
    }

    return cmdtype;

}

void
gen_request_cb(struct evhttp_request *req, void *arg)
{
    fprintf(stdout, "received request:%s\n", (char *)arg);
    
    char *cmdtype;
    enum evhttp_cmd_type cmd;

    struct evhttp_connection *connection;
    const char *host;
    const struct evhttp_uri *evhttp_uri;
    const char *uri;

    struct evbuffer *input_buffer;
    struct evbuffer *output_buffer;
    struct evkeyvalq *input_headers;
    struct evkeyvalq *output_headers;
    struct evkeyval *header;
    int owned;

    const char *uri_fragment;
    const char *uri_host;
    const char *uri_path;
    int uri_port;
    const char *uri_query;
    const char *uri_scheme;
    const char *uri_userinfo;

    struct evbuffer *evb;
    evb = evbuffer_new();
    evbuffer_add_printf(evb, 
                        "<html><head><title>This is a general request</title></head>"
                        "<body><h1>This is general request </h1>"
                        "<h3> Here is the request info:\n</h3>");

    evbuffer_add_printf(evb, "<ul>");

    cmd = evhttp_request_get_command(req);
    cmdtype = parse_evhttp_cmd_type(cmd);

    evbuffer_add_printf(evb,"<li>cmdtype:%s</li>\n", cmdtype);

    if((connection = evhttp_request_get_connection(req)) == NULL){
        perror("evhttp_request_get_connection");
    }

    if((host = evhttp_request_get_host(req)) == NULL){
        perror("evhttp_request_get_host");
    }else{
        evbuffer_add_printf(evb,"<li>host:%s\n</li>", host);
    }

    evhttp_uri = evhttp_request_get_evhttp_uri(req);
    uri = evhttp_request_get_uri(req);
    evbuffer_add_printf(evb, "<li>uri:%s\n</li>", uri);

    /* evbuffer_add_printf(evb, "<li>add input buffer:\r\n"); */
    input_buffer = evhttp_request_get_input_buffer(req);
    /* if((evbuffer_add_buffer(evb, input_buffer)) == -1){ */
    /*     perror("evb add input buffer"); */
    /* } */
    /* evbuffer_add_printf(evb, "</li>"); */

    /* evbuffer_add_printf(evb, "<li>add output buffer:\r\n"); */
    output_buffer = evhttp_request_get_output_buffer(req);
    /* if((evbuffer_add_buffer(evb, output_buffer)) == -1){ */
    /*     perror("evb add output buffer"); */
    /* } */
    /* evbuffer_add_printf(evb, "</li>"); */


    input_headers = evhttp_request_get_input_headers(req);
    evbuffer_add_printf(evb,"<li><ol>current input headers:");
    for(header = input_headers->tqh_first; header; header = header->next.tqe_next){
        evbuffer_add_printf(evb, "<li>%s:%s</li>", header->key, header->value);
    }
    evbuffer_add_printf(evb, "</ol></li>");


    output_headers = evhttp_request_get_output_headers(req);
    evbuffer_add_printf(evb,"<li><ol>current output headers:");
    for(header = output_headers->tqh_first; header; header = header->next.tqe_next){
        evbuffer_add_printf(evb, "<li>%s:%s</li>", header->key, header->value);
    }
    evbuffer_add_printf(evb, "</ol></li>");

    owned = evhttp_request_is_owned(req);
    evbuffer_add_printf(evb, "<li>owned:%d\n</li>", owned);

    uri_fragment = evhttp_uri_get_fragment(evhttp_uri);
    evbuffer_add_printf(evb, "<li>uri_fragment:%s\n</li>", uri_fragment);

    uri_host = evhttp_uri_get_host(evhttp_uri);
    evbuffer_add_printf(evb, "<li>uri_host:%s\n</li>", uri_host);

    uri_path = evhttp_uri_get_path(evhttp_uri);
    evbuffer_add_printf(evb, "<li>uri_path:%s\n</li>", uri_path);

    uri_port = evhttp_uri_get_port(evhttp_uri);
    evbuffer_add_printf(evb, "<li>uri_port:%d\n</li>", uri_port);

    uri_query = evhttp_uri_get_query(evhttp_uri);
    evbuffer_add_printf(evb, "<li>uri_query:%s\n</li>", uri_query);

    uri_scheme = evhttp_uri_get_scheme(evhttp_uri);
    evbuffer_add_printf(evb, "<li>uri_scheme:%s\n</li>", uri_scheme);

    uri_userinfo = evhttp_uri_get_userinfo(evhttp_uri);
    evbuffer_add_printf(evb, "<li>uri_userinfo:%s\n</li>", uri_userinfo);

    evbuffer_add_printf(evb, "</ul>");

    evbuffer_add_printf(evb,"</body></html>");

    evhttp_send_reply(req, 200, "OK", evb);
        

    evbuffer_free(evb);

}

int main(int argc, char **argv)
{
    struct event_base *base;
    struct evhttp *http;
    struct evhttp_bound_socket *handle;

    base = event_base_new();
    http = evhttp_new(base);
    
    evhttp_set_cb(http, "/test", test_request_cb, "test");

    evhttp_set_cb(http, "/html", html_request_cb, NULL);

    evhttp_set_gencb(http, gen_request_cb, WEB_ROOT);

    handle = evhttp_bind_socket_with_handle(http, "0.0.0.0", 8000);

    event_base_dispatch(base);

    return 0;
}
