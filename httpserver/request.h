#ifndef REQUEST_H
#define REQUEST_H

#include <event2/http.h>
#include <event2/event.h>
#include <event2/keyvalq_struct.h>


void get_request_info(struct evhttp_request *req);

void get_request_socket(struct evhttp_request *req);

const char* get_request_host(struct evhttp_request *req);

char* get_request_method(struct evhttp_request *req);

struct evkeyvalq* get_request_input_headers(struct evhttp_request *req);

void add_headers_to_evb(struct evbuffer *evb, struct evkeyvalq *headers);

#endif
