#include "request.h"
#include "utils.h"

void get_request_info(struct evhttp_request *req)
{
    printf("get request info");
}

void get_request_socket(struct evhttp_request *req)
{
    ;
}

const char* get_request_host(struct evhttp_request *req)
{
    return evhttp_request_get_host(req);
}

char* get_request_method(struct evhttp_request *req)
{
    enum evhttp_cmd_type cmd_type = evhttp_request_get_command(req);
    char *cmdtype;
    if(cmd_type){
        cmdtype = evhttp_cmd_type_to_string(cmd_type);

        if(cmdtype){
            return cmdtype;
        }
    }

    return "unknown";
}

struct evkeyvalq* get_request_input_headers(struct evhttp_request *req)
{
    struct evkeyvalq * input_headers;
    input_headers = evhttp_request_get_input_headers(req);
    
    return input_headers;
    
}

void add_headers_to_evb(struct evbuffer *evb, struct evkeyvalq *headers)
{
    struct evkeyval *header;
    
    evbuffer_add_printf(evb, "<ol>headers");

    for(header = headers->tqh_first; header; header = header->next.tqe_next){
        evbuffer_add_printf(evb, "<li>key = %s, value = %s</li>", header->key, header->value);
    }

    evbuffer_add_printf(evb, "</ol>");

}