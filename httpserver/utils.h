#include <event2/event.h>
#include <event2/http.h>

char* evhttp_cmd_type_to_string(enum evhttp_cmd_type cmd_type);

char* guess_mime_from_ext(char* ext);